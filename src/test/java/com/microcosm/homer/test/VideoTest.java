package com.microcosm.homer.test;

import com.microcosm.homer.HomerApplication;
import com.microcosm.homer.model.video.M3U8BO;
import com.microcosm.homer.model.video.NetVideoSearchBO;
import com.microcosm.homer.model.video.TsBO;
import com.microcosm.homer.model.video.TsListBO;
import com.microcosm.homer.service.VideoResolver;
import com.microcosm.homer.service.impl.AiNiVideoResolver;
import com.microcosm.homer.service.impl.CC2VideoResolver;
import com.microcosm.homer.service.impl.V6080VideoResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HomerApplication.class)
public class VideoTest {

    @Resource(name = "aiNiVideoResolver")
    private VideoResolver videoResolver;

    @Test
    public void testVideoResolver() {
        NetVideoSearchBO netVideoSearchBO = videoResolver.search("熊出没");
        M3U8BO m3u8BO = videoResolver.getM3U8(netVideoSearchBO.getNetVideoGroupList().get(4).getNetVideoSourceList().get(0).getNetVideoList().get(0).getUrl());
        TsListBO tsListBO = videoResolver.getTsList(m3u8BO);
        List<TsBO> tsList = tsListBO.getTsList();
        System.out.println(netVideoSearchBO);
    }

}
