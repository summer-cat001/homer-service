package com.microcosm.homer.service.impl;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.microcosm.homer.model.HttpRespBO;
import com.microcosm.homer.model.video.*;
import com.microcosm.homer.service.CommonVideoResolver;
import com.microcosm.homer.utils.Assert;
import com.microcosm.homer.utils.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author summer
 * @date 2024-01-14 17:34
 * 网站因版权问题资源无法下载
 */
@Slf4j
//@Service("v6080VideoResolver")
@Deprecated
public class V6080VideoResolver extends CommonVideoResolver {

    private static final Pattern m3u8UrlPat = Pattern.compile("zanpiancms_player = \\{.*?\"url\":\"(.*?)\"");
    private static final Pattern m3u8NamePat = Pattern.compile("<title>(.*?)</title>");
    private static final Pattern searchContentPat = Pattern.compile("<div class=\"clearfix\" id=\"content\">([\\s\\S]*?)id=\"long-page\">");
    private static final Pattern searchPat = Pattern.compile("<a class=\"video-pic loading\"[\\s\\S]*?href=\"(.*?)\"[\\s\\S]*?title=\"(.*?)\"[\\s\\S]*?data-original=\"(.*?)\"");

    private static final Pattern videoSourcePat = Pattern.compile("<li><a class=\"gico[\\s\\S]*?href=\"#(.*?)\"[\\s\\S]*?>(.*?)</a>");
    private static final Pattern videoLiPat = Pattern.compile("<li[\\s\\S]*?href=\"(.*?)\">(.*?)</a>");

    //guardok Cookie的Max-Age=21600，所以本地缓存6小时;
    private final LoadingCache<Integer, Map<String, String>> headCache = Caffeine.newBuilder()
            .maximumSize(1).expireAfterWrite(6, TimeUnit.HOURS).build(this::antiAntiCrawler);

    public V6080VideoResolver() {
        super("http://m.zhenguangchem.com", "6080影院");
    }

    @Override
    public Map<String, String> getDefaultHeaderMap() {
        //突破反爬虫策略
        return new HashMap<>();//headCache.get(0);
    }

    @Override
    protected Pattern getM3U8UrlPat() {
        return m3u8UrlPat;
    }

    @Override
    protected Pattern getM3U8NamePat() {
        return m3u8NamePat;
    }

    @Override
    protected String resolveSearchContent(String content) {
        Matcher searchContentMatcher = searchContentPat.matcher(content);
        return Optional.of(searchContentMatcher).filter(Matcher::find).map(m -> m.group(1)).orElse(null);
    }

    @Override
    protected List<SearchItemBO> resolveSearchItemList(String searchContent) {
        return resolveSearchItemListTemplate(searchContent, searchPat
                , matcher -> matcher.group(1), matcher -> matcher.group(2), matcher -> matcher.group(3), UnaryOperator.identity());
    }

    @Override
    protected List<SearchSourceItemBO> resolveNetVideoSource(String searchItemPage) {
        return resolvePlayListNetVideoSource(searchItemPage
                , videoSourcePat, matcher -> matcher.group(2), matcher -> matcher.group(1));
    }

    @Override
    protected List<NetVideoBO> resolveNetVideo(String sourceContent) {
        return resolveNetVideoTemplate(sourceContent, videoLiPat
                , matcher -> matcher.group(1), matcher -> matcher.group(2));
    }

    private Map<String, String> antiAntiCrawler(int param) {
        //获取guard
        String url = "http://m.zhenguangchem.com/search/?wd=123";
        HttpRespBO httpRespBO = HttpUtil.httpGet(url);
        Assert.isTrue(httpRespBO, "获取guard失败", () ->
                log.error("V6080VideoResolver getDefaultHeaderMap fail respBO:{}", httpRespBO));

        Map<String, List<String>> headerMap = httpRespBO.getHeaderMap();
        List<String> setCookie = headerMap.get("Set-Cookie");
        Assert.isNotEmpty(setCookie, "V6080VideoResolver 获取guard失败 Set-Cookie 不存在");

        String guard = Arrays.stream(setCookie.get(0).split(";"))
                .filter(c -> c.startsWith("guard=")).findFirst().orElse(null);
        Assert.isNotEmpty(guard, "V6080VideoResolver guard不存在 ");

        //生成guardRet
        String guardRet = getGuardRet(guard.replace("guard=", ""));

        //换取访问凭证guardok
        try {
            //等5秒再执行，否则生成的guardRet服务端解析不了（原网站也是等5秒再去换凭证）
            Thread.sleep(5500);
        } catch (InterruptedException e) {
            log.error("V6080VideoResolver sleep error", e);
        }
        Map<String, String> okHeaderMap = new HashMap<>();
        okHeaderMap.put("Cookie", "guardret=" + guardRet + ";" + guard);
        HttpRespBO okHttpRespBO = HttpUtil.httpGet(url, okHeaderMap);
        Assert.isNotEmpty(okHttpRespBO, "获取guard ok失败");

        Map<String, List<String>> okResHeaderMap = okHttpRespBO.getHeaderMap();
        List<String> okSetCookie = okResHeaderMap.get("Set-Cookie");
        Assert.isNotEmpty(okSetCookie, "V6080VideoResolver 获取guard ok失败 Set-Cookie 不存在");

        String guardOk = Arrays.stream(okSetCookie.get(0).split(";"))
                .filter(c -> c.startsWith("guardok=")).findFirst().orElse(null);
        Assert.isNotEmpty(guardOk, "V6080VideoResolver guard ok不存在 ");

        //返回请求头
        //注意：这个guardOk要搭配者ua一起用，如果后续ua传的和生成guardOk的ua不一致的话也不会通过验签的
        Map<String, String> defaultHeaderMap = new HashMap<>();
        defaultHeaderMap.put("Cookie", guardOk);
        return defaultHeaderMap;
    }

    private String x(String a, String b) {
        b = b + "PTNo2n3Ev5";
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < a.length(); i++) {
            int charCode = Character.codePointAt(a, i) ^ Character.codePointAt(b, i % b.length());
            output.append((char) charCode);
        }
        return output.toString();
    }

    private String getGuardRet(String guard) {
        String b = guard.substring(0, 8);
        String aPre = guard.substring(12);
        int aInt = Integer.parseInt(aPre.substring(10));
        String a = (aInt * 0x2 + 0x11 - 0x2) + "";
        String encrypted = x(a, b);
        Base64.Encoder encoder = Base64.getEncoder();
        return new String(encoder.encode(encrypted.getBytes()));
    }
}
