package com.microcosm.homer.service.impl;

import com.microcosm.homer.model.video.NetVideoBO;
import com.microcosm.homer.model.video.SearchItemBO;
import com.microcosm.homer.model.video.SearchSourceItemBO;
import com.microcosm.homer.service.CommonVideoResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service("aiNiVideoResolver")
public class AiNiVideoResolver extends CommonVideoResolver {
    private static final Pattern m3u8UrlPat = Pattern.compile("var player_aaaa[\\s\\S]*?\"url\":\"(.*?)\"");
    private static final Pattern m3u8NamePat = Pattern.compile("<title>(.*?)</title>");
    private static final Pattern searchPat = Pattern.compile("<a class=\"vodlist_thumb[\\s\\S]*?href=\"(.*?)\"[\\s\\S]*?title=\"(.*?)\"[\\s\\S]*?data-original=\"(.*?)\"");
    private static final Pattern videoSourcePat = Pattern.compile("当前资源由(.*?)提供[\\s\\S]*?playlist_full([\\s\\S]*?)</ul>");
    private static final Pattern videoLiPat = Pattern.compile("<a href=\"(.*?)\">(.*?)</a>");
    private static final Pattern searchDivPat = Pattern.compile("[\\s\\S]*点这里支持一下([\\s\\S]*)");

    public AiNiVideoResolver() {
        super("https://tv.aini.one", "AI NI影院");
    }

    @Override
    public boolean support(String url) {
        return url != null && url.contains("tv.aini.one");
    }

    @Override
    protected String getSearchUrl(String keyword) {
        return sourceUrl + "/index.php/vod/search.html?wd=" + keyword;
    }

    @Override
    protected Pattern getM3U8UrlPat() {
        return m3u8UrlPat;
    }

    @Override
    protected Pattern getM3U8NamePat() {
        return m3u8NamePat;
    }

    @Override
    protected String resolveSearchContent(String content) {
        Matcher searchDivMatcher = searchDivPat.matcher(content);
        if (!searchDivMatcher.find()) {
            return content;
        }
        return searchDivMatcher.group(1);
    }

    @Override
    protected List<SearchItemBO> resolveSearchItemList(String searchContent) {
        return resolveSearchItemListTemplate(searchContent, searchPat, matcher -> matcher.group(1)
                , matcher -> matcher.group(2), matcher -> matcher.group(3), UnaryOperator.identity());
    }

    @Override
    protected List<SearchSourceItemBO> resolveNetVideoSource(String searchItemPage) {
        return resolveNetVideoSourceTemplate(searchItemPage, videoSourcePat
                , matcher -> matcher.group(1), matcher -> matcher.group(2), UnaryOperator.identity());
    }

    @Override
    protected List<NetVideoBO> resolveNetVideo(String sourceContent) {
        return resolveNetVideoTemplate(sourceContent, videoLiPat
                , matcher -> matcher.group(1), matcher -> matcher.group(2));
    }
}
