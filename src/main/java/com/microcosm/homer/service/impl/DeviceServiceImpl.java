package com.microcosm.homer.service.impl;

import com.alibaba.fastjson2.JSON;
import com.microcosm.homer.config.Context;
import com.microcosm.homer.enums.ResultEnum;
import com.microcosm.homer.enums.SSDPStEnum;
import com.microcosm.homer.enums.UPNPActionEnum;
import com.microcosm.homer.model.*;
import com.microcosm.homer.model.video.LocalVideoInfoBO;
import com.microcosm.homer.model.video.PlayTaskBO;
import com.microcosm.homer.model.video.TsAsyncTaskBO;
import com.microcosm.homer.service.DeviceService;
import com.microcosm.homer.service.VideoService;
import com.microcosm.homer.utils.Assert;
import com.microcosm.homer.utils.DataUtils;
import com.microcosm.homer.utils.HttpUtil;
import com.microcosm.homer.utils.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringEscapeUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;

import static com.microcosm.homer.utils.HttpUtil.httpGet;
import static com.microcosm.homer.utils.HttpUtil.httpPostXml;

/**
 * @author summer
 * @date 2022-04-21 19:57
 */
@Slf4j
@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private Context context;

    @Autowired
    private VideoService videoService;

    private final List<PlayTaskBO> playTaskList;

    private Runnable callbackEventRunnable;

    public DeviceServiceImpl() {
        this.playTaskList = new ArrayList<>();
        Thread callbackEventThread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if (callbackEventRunnable != null) {
                        callbackEventRunnable.run();
                        Thread.sleep(10 * 60 * 1000);
                    } else {
                        Thread.sleep(2000);
                    }
                } catch (InterruptedException e) {
                    log.error("callbackEventThread InterruptedException error", e);
                }
            }
        });
        callbackEventThread.setDaemon(true);
        callbackEventThread.setName("callbackEventThread");
        callbackEventThread.start();
    }

    @Override
    public Result<DeviceDescBO> getDeviceDesc(String desUrl) {
        HttpRespBO httpRespBO = httpGet(desUrl);
        return Optional.ofNullable(httpRespBO).map(this::buildDeviceDesc)
                .map(Result::success).orElseGet(() -> Result.fail(ResultEnum.GET_DEVICE_DESC_FAIL));
    }

    @Override
    public Result<Void> setResourceUrl(ActionBO actionBO) {
        String progress = actionBO.getProgress();
        String resourceUrl = actionBO.getResourceUrl();
        String resourceTitle = actionBO.getResourceTitle();

        String metadata = UPNPActionEnum.URI_METADATA.getXmlText();
        metadata = String.format(metadata, resourceTitle, new Date(), resourceUrl);

        String xml = UPNPActionEnum.SET_URI.getXmlText();
        xml = String.format(xml, progress, resourceUrl, StringEscapeUtils.escapeXml10(metadata));
        return executeAction(actionBO, xml);
    }

    @Override
    public Result<Void> playResource(ActionBO actionBO) {
        String speed = actionBO.getSpeed();
        String progress = actionBO.getProgress();
        String xml = UPNPActionEnum.PLAY.getXmlText();
        xml = String.format(xml, progress, speed);
        return executeAction(actionBO, xml);
    }

    @Override
    public Result<String> getPositionInfo(ActionBO actionBO) {
        String xml = UPNPActionEnum.GET_POSITION_INFO.getXmlText();
        Result<String> result = executeAction(actionBO, xml, httpRespBO -> Result.success(httpRespBO.getUTF8Body()));
        return result;
    }

    @Override
    public Result<Void> playVideo(int deviceId, String videoId) {
        List<DeviceDescBO> deviceDescList = context.getDeviceDescList();
        Assert.isTrue(deviceDescList != null, "未搜索投屏设备");
        Assert.isTrue(deviceId < deviceDescList.size(), "设备id错误");
        DeviceDescBO deviceDescBO = deviceDescList.get(deviceId);

        List<ServiceBO> serviceList = deviceDescBO.getServiceList();
        Assert.isNotEmpty(serviceList, "设备服务不存在");

        Optional<ServiceBO> serviceOptional = serviceList.stream().filter(s ->
                SSDPStEnum.AV_TRANSPORT_V1.getType().equals(s.getServiceType())).findFirst();
        Assert.isTrue(serviceOptional.isPresent(), "投屏服务不存在");

        ServiceBO serviceBO = serviceOptional.get();
        String controlUrl = serviceBO.getControlUrl();
        controlUrl = controlUrl.startsWith("/") ? controlUrl.substring(1) : controlUrl;

        LocalVideoInfoBO localVideoInfoBO = videoService.getLocalVideoInfo(videoId).getData();
        String videoName = Optional.ofNullable(localVideoInfoBO).map(LocalVideoInfoBO::getName).orElse(null);

        ActionBO urlAction = new ActionBO();
        urlAction.setProgress("0");
        urlAction.setResourceTitle(videoName);
        urlAction.setResourceUrl(context.getLocalHost() + "/video/m3u8/" + videoId);
        urlAction.setSoapAction("\"" + serviceBO.getServiceType() + "#SetAVTransportURI\"");
        urlAction.setActionUrl(NetUtil.resolveRootUrl(deviceDescBO.getUrl()) + "/" + controlUrl);
        Result<Void> result = setResourceUrl(urlAction);
        Assert.isTrue(result.isSuccess(), result.getCode(), result.getMessage());
        urlAction.setSoapAction("\"" + serviceBO.getServiceType() + "#Play\"");
        urlAction.setSpeed("1");
        urlAction.setProgress("0");
        return playResource(urlAction);
    }

    @Override
    public Result<Void> addDevicePlayTask(String videoId) {
        Result<LocalVideoInfoBO> result = videoService.getLocalVideoInfo(videoId);
        if (result.failed()) {
            return Result.fail("视频不存在");
        }
        LocalVideoInfoBO localVideoInfoBO = result.getData();
        if (!"100.00%".equals(localVideoInfoBO.getRate())) {
            return Result.fail("视频未下载完成");
        }

        PlayTaskBO playTaskBO = new PlayTaskBO();
        playTaskBO.setVideoId(videoId);
        playTaskBO.setName(localVideoInfoBO.getName());
        addPlayTask(playTaskBO);
        return Result.success(null);
    }

    @Override
    public Result<Void> rmDevicePlayTask(String videoId, int index) {
        rmPlayTask(videoId, index);
        return Result.success(null);
    }

    @Override
    public Result<Void> changeDevicePlayTaskOrder(String videoId, int index, int action) {
        orderPlayTask(videoId, index, action);
        return Result.success(null);
    }

    @Override
    public synchronized Result<Void> playPlayTask(int deviceId) {
        PlayTaskBO playTaskBO = getFirstPlayTask();
        if (playTaskBO == null) {
            return Result.fail("无播放任务");
        }
        String videoId = playTaskBO.getVideoId();
        Result<Void> result = playVideo(deviceId, videoId);
        if (result.isSuccess()) {
            rmPlayTask(videoId, 0);
            List<DeviceDescBO> deviceDescList = context.getDeviceDescList();
            if (DataUtils.isNotEmpty(deviceDescList)) {
                DeviceDescBO deviceDescBO = deviceDescList.get(deviceId);
                List<ServiceBO> serviceList = deviceDescBO.getServiceList();
                Optional<ServiceBO> serviceOptional = serviceList.stream().filter(s ->
                        SSDPStEnum.AV_TRANSPORT_V1.getType().equals(s.getServiceType())).findFirst();
                if (serviceOptional.isPresent()) {
                    ServiceBO serviceBO = serviceOptional.get();
                    String eventSubUrl = serviceBO.getEventSubUrl();
                    eventSubUrl = eventSubUrl.startsWith("/") ? eventSubUrl.substring(1) : eventSubUrl;
                    String callbackEventUrl = NetUtil.resolveRootUrl(deviceDescBO.getUrl()) + "/" + eventSubUrl;

                    Map<String, String> headerMap = new HashMap<>();
                    headerMap.put("NT", "upnp:event");
                    headerMap.put("TIMEOUT", "Second-3600");
                    headerMap.put("CALLBACK", "<" + context.getLocalHost() + "/device/callback>");
                    callbackEventRunnable = () -> {
                        HttpRespBO respBO = HttpUtil.httpSubscribe(callbackEventUrl, headerMap, null);
                        log.info("playPlayTask callback respBO:{}", respBO);
                    };
                    callbackEventRunnable.run();
                }
            }
        }
        return result;
    }

    @Override
    public Result<List<PlayTaskBO>> getTaskList() {
        return Result.success(playTaskList);
    }

    private synchronized void addPlayTask(PlayTaskBO playTaskBO) {
        playTaskList.add(playTaskBO);
    }

    //双重校验，防止删除重复任务中的一个把所有重复任务都删了
    private synchronized void rmPlayTask(String videoId, int index) {
        Optional.of(index).map(playTaskList::get).filter(task ->
                task.getVideoId().equals(videoId)).ifPresent(t -> playTaskList.remove(index));
    }

    private synchronized void orderPlayTask(String videoId, int index, int action) {
        Optional.of(index).map(playTaskList::get).filter(task -> task.getVideoId().equals(videoId)).ifPresent(t -> {
            int finalIndex = index + action;
            if (finalIndex < 0) {
                finalIndex = playTaskList.size() - 1;
            }
            if (finalIndex >= playTaskList.size()) {
                finalIndex = 0;
            }
            playTaskList.remove(index);
            playTaskList.add(finalIndex, t);
        });
    }

    private synchronized PlayTaskBO getFirstPlayTask() {
        return playTaskList.isEmpty() ? null : playTaskList.get(0);
    }

    private Result<Void> executeAction(ActionBO actionBO, String xml) {
        return executeAction(actionBO, xml, httpRespBO -> Result.success(null));
    }

    private <T> Result<T> executeAction(ActionBO actionBO, String xml, Function<HttpRespBO, Result<T>> function) {
        String actionUrl = actionBO.getActionUrl();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("SOAPACTION", actionBO.getSoapAction());
        HttpRespBO httpRespBO = httpPostXml(actionUrl, xml, headerMap);
        return Optional.ofNullable(httpRespBO).filter(HttpRespBO::success).map(function).orElseGet(() -> {
            log.error("执行动作失败,{},{},{}", actionUrl, xml, httpRespBO);
            return Result.fail("执行动作失败");
        });
    }

    //构建设备的描述和其服务列表信息
    private DeviceDescBO buildDeviceDesc(HttpRespBO httpRespBO) {
        try {
            if (!httpRespBO.ok()) {
                log.error("设备描述响应错误:{}", JSON.toJSONString(httpRespBO));
                return null;
            }
            String xml = httpRespBO.getUTF8Body();
            DeviceDescBO deviceDescBO = new DeviceDescBO();
            deviceDescBO.setServiceList(new ArrayList<>());
            Document doc = DocumentHelper.parseText(xml);
            Element rootElt = doc.getRootElement();
            Element recordEle = rootElt.element("device");
            Element serviceList = recordEle.element("serviceList");
            Iterator<?> iterator = serviceList.elementIterator("service");
            deviceDescBO.setDeviceType(recordEle.elementTextTrim("deviceType"));
            deviceDescBO.setFriendlyName(recordEle.elementTextTrim("friendlyName"));
            while (iterator.hasNext()) {
                ServiceBO serviceVO = new ServiceBO();
                deviceDescBO.getServiceList().add(serviceVO);
                Element serviceElement = (Element) iterator.next();
                serviceVO.setScpDUrl(serviceElement.elementTextTrim("SCPDURL"));
                serviceVO.setServiceId(serviceElement.elementTextTrim("serviceId"));
                serviceVO.setControlUrl(serviceElement.elementTextTrim("controlURL"));
                serviceVO.setServiceType(serviceElement.elementTextTrim("serviceType"));
                serviceVO.setEventSubUrl(serviceElement.elementTextTrim("eventSubURL"));
            }
            return deviceDescBO;
        } catch (DocumentException e) {
            log.error("设备描述响应解析失败:{}", JSON.toJSONString(httpRespBO), e);
            return null;
        }
    }
}
