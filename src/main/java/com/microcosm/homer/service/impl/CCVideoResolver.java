package com.microcosm.homer.service.impl;

import com.microcosm.homer.model.HttpRespBO;
import com.microcosm.homer.model.video.*;
import com.microcosm.homer.service.CommonVideoResolver;
import com.microcosm.homer.utils.HttpUtil;
import com.microcosm.homer.utils.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author summer
 * @date 2023-12-09 11:01
 */
@Slf4j
@Service("ccVideoResolver")
public class CCVideoResolver extends CommonVideoResolver {

    private static final Pattern m3u8NamePat = Pattern.compile("<title>(.*?)</title>");
    private static final Pattern m3u8UrlPat = Pattern.compile("player_data=\\{.*?\"url\":\"(.*?)\"");
    private static final Pattern searchDivPat = Pattern.compile("<div class=\"pannel search_box clearfix\">[\\s\\S]*?</ul>");
    private static final Pattern searchPat = Pattern.compile("<a class=\"vodlist_thumb lazyload\" href=\"(.*?)\"[\\s\\S]*?title=\"(.*?)\"[\\s\\S]*?data-original=\"(.*?)\"");
    private static final Pattern playPagePat = Pattern.compile("<a class=\"btn btn_primary\" href=\"(.*?)\"");
    private static final Pattern videoSourcePat = Pattern.compile("<i class=\"iconfont\">&#xe62f;</i>&nbsp;(.*?)</a>[\\s\\S]*?</ul>");
    private static final Pattern videoLiPat = Pattern.compile("<li[\\s\\S]*?href=\"(.*?)\">(.*?)</a>");

    public CCVideoResolver() {
        super("https://www.nxyjjt.com", "策驰影院");
    }

    @Override
    protected Pattern getM3U8UrlPat() {
        return m3u8UrlPat;
    }

    @Override
    protected Pattern getM3U8NamePat() {
        return m3u8NamePat;
    }

    @Override
    protected String resolveSearchContent(String content) {
        Matcher searchDivMatcher = searchDivPat.matcher(content);
        if (!searchDivMatcher.find()) {
            return null;
        }
        return searchDivMatcher.group();
    }

    @Override
    protected List<SearchItemBO> resolveSearchItemList(String searchContent) {
        return resolveSearchItemListTemplate(searchContent, searchPat, matcher -> matcher.group(1)
                , matcher -> matcher.group(2), matcher -> matcher.group(3), searchItemBO -> {
                    String playPageUrl = searchItemBO.getUrl();
                    HttpRespBO pageRespBO = HttpUtil.httpGet(playPageUrl, 3);
                    if (pageRespBO == null || !pageRespBO.ok()) {
                        log.error("ccVideoResolver playPage  fail url:{},pageRespBO:{}", playPageUrl, pageRespBO);
                        return null;
                    }
                    String playPage = pageRespBO.getUTF8Body();
                    Matcher playPageMatcher = playPagePat.matcher(playPage);
                    if (!playPageMatcher.find()) {
                        log.error("ccVideoResolver playPageMatcher fail url:{},pageRespBO:{}", playPageUrl, pageRespBO);
                        return null;
                    }
                    searchItemBO.setUrl(NetUtil.composeUrl(sourceUrl, playPageMatcher.group(1)));
                    return searchItemBO;
                });
    }

    @Override
    protected List<SearchSourceItemBO> resolveNetVideoSource(String searchItemPage) {
        return resolveNetVideoSourceTemplate(searchItemPage, videoSourcePat
                , matcher -> matcher.group(1), Matcher::group, UnaryOperator.identity());
    }

    @Override
    protected List<NetVideoBO> resolveNetVideo(String sourceContent) {
        return resolveNetVideoTemplate(sourceContent, videoLiPat
                , matcher -> matcher.group(1), matcher -> matcher.group(2));
    }
}
