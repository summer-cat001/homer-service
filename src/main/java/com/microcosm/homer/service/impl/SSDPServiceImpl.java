package com.microcosm.homer.service.impl;

import com.microcosm.homer.config.Context;
import com.microcosm.homer.enums.ResultEnum;
import com.microcosm.homer.model.*;
import com.microcosm.homer.service.DeviceService;
import com.microcosm.homer.service.SSDPService;
import com.microcosm.homer.enums.SSDPStEnum;
import com.microcosm.homer.utils.ArrayExtraUtil;
import com.microcosm.homer.utils.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static com.microcosm.homer.model.SSDPReqBO.buildDiscover;

/**
 * @author summer
 * @date 2022-04-21 11:54
 */
@Slf4j
@Service
public class SSDPServiceImpl implements SSDPService {

    @Autowired
    private Context context;

    @Autowired
    private DeviceService deviceService;

    @Value("${ssdp.timeout:2000}")
    private long timeout;

    private Thread notifyThread;
    private final Set<SSDPStEnum> notifyServiceTypes = new HashSet<>();
    public static final List<DeviceDescBO> notifyDeviceList = new ArrayList<>();
    private final ExecutorService executor = new ThreadPoolExecutor(0, 5,
            0, TimeUnit.SECONDS, new SynchronousQueue<>(), new ThreadPoolExecutor.CallerRunsPolicy());

    @Override
    public Result<List<DeviceDescBO>> discover(SSDPStEnum ssdpStEnum) {
        List<DeviceDescBO> list = new ArrayList<>();
        SSDPReqBO ssdpReqBO = buildDiscover(ssdpStEnum);
        try (DatagramSocket udpSocket = new DatagramSocket()) {
            int port = ssdpReqBO.getSsdpPort();
            InetAddress address = InetAddress.getByName(ssdpReqBO.getSsdpIp());
            byte[] body = ssdpReqBO.toString().getBytes(StandardCharsets.UTF_8);
            //用udp像组播地址发送http请求，等待能接受该请求的服务响应
            udpSocket.send(new DatagramPacket(body, body.length, address, port));
            receiveSSDP(udpSocket, r -> executor.execute(() -> setDeviceDesc(r, list)));
            return Result.success(list);
        } catch (SocketTimeoutException e) {
            return Result.success(list);
        } catch (Exception e) {
            log.error("组播ssdp搜索设备失败", e);
            return list.isEmpty() ? Result.fail(ResultEnum.SSDP_SEARCH_FAIL) : Result.success(list);
        } finally {
            context.setDeviceDescList(list);
        }
    }

    @Override
    public synchronized Result<Void> receiveNotify(SSDPStEnum ssdpStEnum) {
        notifyServiceTypes.add(ssdpStEnum);
        if (notifyThread == null) {
            notifyThread = new Thread(this::runNotify);
            notifyThread.setDaemon(true);
            notifyThread.setName("notifyThread");
            notifyThread.start();
        }
        return Result.empty();
    }

    @Override
    public synchronized Result<Void> stopReceiveNotify(SSDPStEnum ssdpStEnum) {
        notifyServiceTypes.remove(ssdpStEnum);
        if (notifyThread != null && notifyServiceTypes.isEmpty()) {
            notifyThread.interrupt();
            notifyThread = null;
        }
        return Result.empty();
    }

    private void runNotify() {
        log.info("ssdp notify监听开始");
        //构建一个服务加入组播，监听服务上线和下线事件
        try (MulticastSocket socket = new MulticastSocket(1900)) {
            socket.joinGroup(InetAddress.getByName("239.255.255.250"));
            while (!Thread.currentThread().isInterrupted()) {
                receiveSSDP(socket, this::runNotify);
            }
        } catch (Exception e) {
            log.error("ssdp notify异常", e);
        } finally {
            log.info("ssdp notify监听结束");
        }
    }

    //notifyDeviceList只有一个线程操作，没有并发问题
    private void runNotify(SSDPRespBO ssdpRespBO) {
        if (ssdpRespBO != null) {
            String nts = ssdpRespBO.getNts();
            String url = ssdpRespBO.getLocation();
            SSDPStEnum nt = SSDPStEnum.getEnumByType(ssdpRespBO.getNt());
            if (nts.equals("ssdp:alive") && notifyServiceTypes.contains(nt) &&
                    notifyDeviceList.stream().map(DeviceDescBO::getUrl).noneMatch(url::equals)) {
                setDeviceDesc(ssdpRespBO, notifyDeviceList);
            }
            if (nts.equals("ssdp:byebye")) {
                notifyDeviceList.removeIf(deviceDescBO -> deviceDescBO.getUrl().equals(url));
            }
        }
    }

    private void receiveSSDP(DatagramSocket udpSocket, Consumer<SSDPRespBO> consumer) throws IOException {
        //udf一次从socket内核缓冲区能取到一整个包，和tcp获取流不一样
        // 如果data设置的太小只能获取一部分字节，包剩下的字节也会丢弃
        // 所以data要设置的大于等于一个包的大小，这里我设置为udf包的最大长度
        byte[] data = new byte[65507];
        long endTime = System.currentTimeMillis() + timeout;
        DatagramPacket dp = new DatagramPacket(data, data.length);
        while (endTime - System.currentTimeMillis() > 0) {
            udpSocket.setSoTimeout((int) timeout);
            udpSocket.receive(dp);
            //本次接收到的数据的实际长度，从索引0开始覆盖data数组
            int length = dp.getLength();
            String str = new String(data, 0, length);
            consumer.accept(buildSSDPResp(str));
            //设置下次读取的最大长度，否则会使用上次接收到的字节长度，receive会设置length属性
            dp.setLength(data.length);
        }
    }

    private void setDeviceDesc(SSDPRespBO ssdpRespBO, List<DeviceDescBO> list) {
        if (ssdpRespBO != null) {
            String location = ssdpRespBO.getLocation();
            Result<DeviceDescBO> result = deviceService.getDeviceDesc(location);
            if (result.isSuccess()) {
                DeviceDescBO deviceDescBO = result.getData();
                deviceDescBO.setUrl(location);
                list.add(deviceDescBO);
            }
        }
    }

    private SSDPRespBO buildSSDPResp(String resp) {
        String[] respArray = resp.split("\r\n");
        if (!respArray[0].contains(" 200 OK")) {
            log.error("响应失败:{}", resp);
            return null;
        }
        SSDPRespBO ssdpRespBO = new SSDPRespBO();
        buildSSDPResp(Arrays.stream(respArray), ssdpRespBO);
        return ssdpRespBO;
    }

    private void buildSSDPResp(Stream<String> stream, SSDPRespBO ssdpRespBO) {
        stream.skip(1).filter(h -> h.contains(":")).forEach(item -> {
            int splitIndex = item.indexOf(":");
            String key = item.substring(0, splitIndex).trim().toLowerCase();
            Optional.of(key).map(SSDPRespBO.biConsumerMap::get).ifPresent(biConsumer ->
                    biConsumer.accept(ssdpRespBO, item.substring(splitIndex + 1).trim()));
        });
    }
}
