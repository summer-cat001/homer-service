package com.microcosm.homer.service.impl;

import com.microcosm.homer.model.Result;
import com.microcosm.homer.model.event.StopTsTaskEvent;
import com.microcosm.homer.model.video.*;
import com.microcosm.homer.service.VideoActuator;
import com.microcosm.homer.service.VideoResolver;
import com.microcosm.homer.service.VideoService;
import com.microcosm.homer.utils.Assert;
import com.microcosm.homer.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author summer
 * @date 2023-12-05 16:05
 */
@Slf4j
@Service
public class VideoServiceImpl implements VideoService {

    @Resource(name = "commonVideoActuator")
    private VideoActuator videoActuator;

    @Resource(name = "searchVideoPool")
    private ExecutorService searchVideoPool;

    @Resource(name = "improveVideoPool")
    private ExecutorService improveVideoPool;

    @Autowired
    private List<VideoResolver> videoResolvers;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${video.base.path}")
    private String videoBasePath;

    private String executingFileId;
    private final Thread tsAsyncTaskThread;
    private final List<TsAsyncTaskBO> tsAsyncTaskList;
    private final Map<Pattern, BiConsumer<LocalVideoInfoBO, String>> videoInfoMap;

    public VideoServiceImpl() {
        videoInfoMap = new HashMap<>();
        videoInfoMap.put(Pattern.compile("进度：(.*)"), LocalVideoInfoBO::setRate);
        videoInfoMap.put(Pattern.compile("文件名：(.*)"), LocalVideoInfoBO::setName);
        videoInfoMap.put(Pattern.compile("来源：(.*)"), LocalVideoInfoBO::setSource);
        videoInfoMap.put(Pattern.compile(VideoActuator.TS_NUM + "(.*)"), LocalVideoInfoBO::setTsNum);

        tsAsyncTaskList = new ArrayList<>();

        tsAsyncTaskThread = new Thread(this::executeTsAsyncTask);
        tsAsyncTaskThread.setDaemon(true);
        tsAsyncTaskThread.setName("tsAsyncTaskThread");
        tsAsyncTaskThread.start();
    }

    @Override
    public Result<byte[]> getFileByte(String relativePath) {
        String filePath = videoBasePath + "/" + relativePath;
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(filePath, "r")) {
            byte[] buffer = new byte[(int) randomAccessFile.length()];
            randomAccessFile.read(buffer);
            return Result.success(buffer);
        } catch (Exception e) {
            log.error("getTs fail,filePath:{}", filePath, e);
            return Result.fail("获取文件失败");
        }
    }

    @Override
    public Result<LocalVideoInfoBO> getLocalVideoInfo(String videoId) {
        String infoPath = videoBasePath + "/" + videoId + "/info.txt";
        Path path = Paths.get(infoPath);
        if (!Files.exists(path)) {
            return Result.fail("info文件不存在");
        }
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(infoPath, "r");
             FileChannel fileChannel = randomAccessFile.getChannel()) {
            boolean improveIng = videoActuator.isImproveIng(videoId);
            LocalVideoInfoBO localVideoInfoBO = new LocalVideoInfoBO();
            localVideoInfoBO.setId(videoId);
            localVideoInfoBO.setImproveIng(improveIng);
            //加文件读锁，如果系统不支持，会升级为排他锁
            FileLock fileLock = fileChannel.lock(0, Long.MAX_VALUE, true);
            try {
                List<String> lines = Files.readAllLines(path);
                lines.forEach(line -> videoInfoMap.forEach((pattern, consumer) -> {
                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        consumer.accept(localVideoInfoBO, matcher.group(1));
                    }
                }));
            } finally {
                fileLock.release();
            }
            return Result.success(localVideoInfoBO);
        } catch (Exception e) {
            log.error("get video info fail,infoPath:{}", infoPath, e);
            return Result.fail("获取info文件失败");
        }
    }

    public static void main(String[] args) throws IOException {

        try (RandomAccessFile randomAccessFile = new RandomAccessFile("/Users/admin/IdeaProjects/video/e84835097d4545adab06c074c935f665/origin2.m3u8", "rw")) {
            String localM3U8 = new String(Files.readAllBytes(Paths.get("/Users/admin/IdeaProjects/video/e84835097d4545adab06c074c935f665/origin2.m3u8")), StandardCharsets.UTF_8);
            randomAccessFile.write((localM3U8+"123").getBytes("utf-8"));
        }

    }
    @Override
    public Result<List<NetVideoSearchBO>> searchVideo(String keyword) {
        List<Future<NetVideoSearchBO>> futureList = videoResolvers.stream().map(vr ->
                searchVideoPool.submit(() -> vr.search(keyword))).collect(Collectors.toList());

        return Result.success(futureList.stream().map(future -> {
            try {
                return future.get();
            } catch (Exception e) {
                log.error("searchVideo future get error", e);
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList()));
    }

    @Override
    public Result<String> crawlVideo(String url) {
        return videoResolvers.stream().filter(vr -> vr.support(url))
                .map(vr -> videoActuator.downloadAndSaveTS(url, vr))
                .findFirst().orElseGet(() -> Result.fail("该地址不支持解析"));
    }

    @Override
    public Result<Void> deleteVideo(String videoId) {
        if (!FileUtil.deleteFolder(videoBasePath + "/" + videoId)) {
            return Result.fail("视频删除失败");
        }
        return Result.success(null);
    }

    @Override
    public Result<Void> improveVideo(String videoId) {
        Result<LocalVideoInfoBO> result = getLocalVideoInfo(videoId);
        if (result.failed()) {
            return Result.fail("视频不存在");
        }
        LocalVideoInfoBO localVideoInfoBO = result.getData();
        List<Integer> tsNum = localVideoInfoBO.getTsNum();
        if (tsNum == null) {
            return Result.fail("视频下载版本过低，请删除重新下载");
        }
        VideoResolver videoResolver = videoResolvers.stream().filter(vr ->
                vr.support(localVideoInfoBO.getSource())).findFirst().orElse(null);
        Assert.isTrue(videoResolver != null, "视频解析器不存在，请重新下载");
        videoActuator.improveVideo(localVideoInfoBO, videoResolver);
        return Result.success(null);
    }

    @Override
    public Result<String> addCrawlVideoAsync(String url) {
        Result<TsAsyncTaskBO> tsAsyncTaskResult = videoResolvers.stream().filter(vr -> vr.support(url))
                .map(vr -> videoActuator.downloadAndSaveTSAsync(url, vr)).findFirst().orElse(null);
        if (tsAsyncTaskResult == null) {
            log.error("生成下载任务失败 url:{}", url);
            return null;
        }
        if (tsAsyncTaskResult.failed()) {
            log.error("生成下载任务失败 url:{},tsAsyncTaskResult:{}", url, tsAsyncTaskResult);
            return null;
        }
        TsAsyncTaskBO tsAsyncTaskBO = tsAsyncTaskResult.getData();
        addTsAsyncTask(tsAsyncTaskBO);
        return Result.success(tsAsyncTaskBO.getFileId());
    }

    @Override
    public synchronized Result<Void> rmCrawlVideoAsync(String fileId) {
        //如果删除正在执行的下载任务，需要终止正在运行中的线程
        if (fileId.equals(executingFileId)) {
            //发布终止任务事件
            //因为事件是同步的，并且操作executingFileId或者下载任务队列都加了同一个锁，所以无需考虑并发问题
            applicationContext.publishEvent(new StopTsTaskEvent(this));
            deleteVideo(fileId);
        }
        rmTsAsyncTask(fileId);
        return Result.success(null);
    }

    @Override
    public Result<List<TsAsyncTaskVO>> getTsAsyncTaskList() {
        return Result.success(tsAsyncTaskList.stream().map(tsAsyncTask -> {
            String fileId = tsAsyncTask.getFileId();
            Result<LocalVideoInfoBO> result = getLocalVideoInfo(fileId);

            TsAsyncTaskVO tsAsyncTaskVO = new TsAsyncTaskVO();
            tsAsyncTaskVO.setFileId(fileId);
            tsAsyncTaskVO.setUrl(tsAsyncTask.getUrl());
            tsAsyncTaskVO.setName(tsAsyncTask.getName());
            tsAsyncTaskVO.setRate(result.failed() ? "0.00%" : result.getData().getRate());
            return tsAsyncTaskVO;
        }).collect(Collectors.toList()));
    }

    @Override
    public Result<List<LocalVideoInfoBO>> getVideoList() {
        File[] files;
        File folder = new File(videoBasePath);
        if (!folder.exists() || (files = folder.listFiles()) == null) {
            return Result.success(new ArrayList<>());
        }
        return Result.success(Arrays.stream(files).map(file -> {
            String infoPath = file.getAbsolutePath() + "/info.txt";
            if (!(new File(infoPath)).exists()) {
                return null;
            }
            String videoId = file.getName();
            Result<LocalVideoInfoBO> result = getLocalVideoInfo(videoId);
            if (result.failed()) {
                return null;
            }
            LocalVideoInfoBO localVideoInfoBO = result.getData();
            if (!"100.00%".equals(localVideoInfoBO.getRate())) {
                return null;
            }
            return localVideoInfoBO;
        }).filter(Objects::nonNull).sorted(Comparator.comparing(LocalVideoInfoBO::getName)).collect(Collectors.toList()));
    }

    @EventListener
    public void handleMyEvent(StopTsTaskEvent event) {
        tsAsyncTaskThread.interrupt();
    }

    private void executeTsAsyncTask() {
        while (true) {
            TsAsyncTaskBO tsAsyncTaskBO = getFirstTsAsyncTask();
            try {
                if (tsAsyncTaskBO == null) {
                    Thread.sleep(2000);
                    continue;
                }
                executingFileId = tsAsyncTaskBO.getFileId();
                tsAsyncTaskBO.getRunnable().run();
            } catch (InterruptedException ignored) {
            } catch (Exception e) {
                log.error("executeTsAsyncTask error", e);
            } finally {
                rmTsAsyncTask(executingFileId);
                Thread.interrupted();//清除中断位，以至该线程可以继续被使用，中断只是为了结束正在执行的任务
            }
        }
    }

    private synchronized void addTsAsyncTask(TsAsyncTaskBO tsAsyncTask) {
        tsAsyncTaskList.add(tsAsyncTask);
    }

    private synchronized void rmTsAsyncTask(String fileId) {
        tsAsyncTaskList.removeIf(task -> task.getFileId().equals(fileId));
    }

    private synchronized TsAsyncTaskBO getFirstTsAsyncTask() {
        return tsAsyncTaskList.isEmpty() ? null : tsAsyncTaskList.get(0);
    }

}
