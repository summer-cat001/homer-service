package com.microcosm.homer.service.impl;

import com.alibaba.fastjson.JSON;
import com.microcosm.homer.exception.ViewException;
import com.microcosm.homer.model.HttpRespBO;
import com.microcosm.homer.model.Result;
import com.microcosm.homer.model.event.StopTsTaskEvent;
import com.microcosm.homer.model.video.*;
import com.microcosm.homer.service.VideoActuator;
import com.microcosm.homer.service.VideoResolver;
import com.microcosm.homer.utils.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author summer
 * @date 2023-12-05 14:54
 * 通用视频执行器
 */
@Slf4j
@Service("commonVideoActuator")
public class CommonVideoActuator implements VideoActuator {

    @Resource(name = "downloadTSPool")
    private ExecutorService downloadTSPool;

    @Resource(name = "improveVideoPool")
    private ExecutorService improveVideoPool;

    @Value("${video.base.path}")
    private String videoBasePath;

    private final List<String> improveFidList = new Vector<>();
    public static final Pattern tsNumPat = Pattern.compile(TS_NUM + "(.*)\n");
    public static final Pattern encKeyPat = Pattern.compile("#EXT-X-KEY.*?URI=\"(.*?)\"[^\n]*");

    @Override
    public Result<String> downloadAndSaveTS(String url, VideoResolver videoResolver) {

        try {
            M3U8BO m3u8BO = videoResolver.getM3U8(url);
            return downloadAndSaveTS(m3u8BO, videoResolver);
        } catch (ViewException e) {
            throw e;
        } catch (Exception e) {
            log.error("downloadAndSaveTS fail url:{}", url, e);
            return Result.fail("下载保存视频发生错误");
        }
    }

    @Override
    public Result<TsAsyncTaskBO> downloadAndSaveTSAsync(String url, VideoResolver videoResolver) {
        try {
            M3U8BO m3u8BO = videoResolver.getM3U8(url);
            TsAsyncTaskBO tsAsyncTaskBO = new TsAsyncTaskBO();
            tsAsyncTaskBO.setUrl(url);
            tsAsyncTaskBO.setFileId(m3u8BO.getId());
            tsAsyncTaskBO.setName(m3u8BO.getName());
            tsAsyncTaskBO.setRunnable(() -> downloadAndSaveTS(m3u8BO, videoResolver));
            return Result.success(tsAsyncTaskBO);
        } catch (Exception e) {
            log.error("downloadAndSaveTSAsync error,url:{}", url, e);
            return Result.fail("生成下载任务失败");
        }
    }

    @Override
    public void improveVideo(LocalVideoInfoBO localVideoInfoBO, VideoResolver videoResolver) {
        String videoId = localVideoInfoBO.getId();
        synchronized (videoId.intern()) {
            Assert.isTrue(!isImproveIng(videoId), "视频正在完善，请勿重复操作");
            String basePath = videoBasePath + "/" + videoId;
            try {
                Path path = Paths.get(basePath + "/local.m3u8");
                Assert.isTrue(Files.exists(path), "local.m3u8文件不存在");
                List<String> lines = Files.readAllLines(path);
                lines.removeIf(line -> !line.startsWith(VideoActuator.HOMER_HLS_TS_PRE));
                if (lines.isEmpty()) {
                    return;
                }
                improveFidList.add(videoId);
                AtomicInteger improveNum = new AtomicInteger(lines.size());
                UnaryOperator<byte[]> decodeFunction = getLocalDecodeTsFunction(videoId, videoResolver);
                lines.forEach(line -> {
                    String metadataStr = line.replaceFirst(VideoActuator.HOMER_HLS_TS_PRE, "");
                    List<String> tsMetadata = JSON.parseArray(metadataStr, String.class);
                    improveVideoPool.submit(() -> {
                        String index = tsMetadata.get(0);
                        String tsUrl = tsMetadata.get(1);
                        String tsFilePath = basePath + "/ts/" + index + ".ts";
                        String localTsPath = "/video/ts/" + videoId + "/" + index + ".ts";

                        boolean res = doDownloadAndSaveTS(tsUrl, tsFilePath, decodeFunction);
                        synchronized (improveNum) {
                            Optional.of(res).filter(Boolean.TRUE::equals).ifPresent(r ->
                                    improveVideoPost(basePath, line, localTsPath, index));

                            Optional.of(improveNum).map(AtomicInteger::decrementAndGet)
                                    .filter(num -> num == 0).ifPresent(r -> improveFidList.remove(videoId));
                        }
                    });
                });
            } catch (Exception e) {
                log.error("improveVideo fail,videoId:{}", videoId, e);
            }
        }
    }

    @Override
    public boolean isImproveIng(String videoId) {
        return improveFidList.contains(videoId);
    }

    private UnaryOperator<byte[]> getLocalDecodeTsFunction(String videoId, VideoResolver videoResolver) throws IOException {
        String basePath = videoBasePath + "/" + videoId;
        Path encKeyPath = Paths.get(basePath + "/enc.key");
        UnaryOperator<byte[]> decodeTsFunction = null;
        if (Files.exists(encKeyPath)) {
            byte[] encKey = Files.readAllBytes(encKeyPath);
            Path originM3U8Path = Paths.get(basePath + "/origin.m3u8");
            String originM3U8 = new String(Files.readAllBytes(originM3U8Path));
            decodeTsFunction = videoResolver.getDecodeTsFunction(originM3U8, encKey);
        }
        return Optional.ofNullable(decodeTsFunction).orElse(UnaryOperator.identity());
    }

    private void improveVideoPost(String basePath, String line, String localTsPath, String index) {
        try {
            Path localM3U8Path = Paths.get(basePath + "/local.m3u8");
            String localM3U8 = new String(Files.readAllBytes(localM3U8Path), StandardCharsets.UTF_8);
            localM3U8 = localM3U8.replace(line, localTsPath).replace(HOMER_HLS_PRE + index + "-", "");
            Files.write(localM3U8Path, localM3U8.getBytes(StandardCharsets.UTF_8));

            String infoPath = basePath + "/info.txt";
            try (RandomAccessFile randomAccessFile = new RandomAccessFile(infoPath, "rw");
                 FileChannel fileChannel = randomAccessFile.getChannel()) {
                FileLock fileLock = fileChannel.lock();
                try {
                    String info = new String(Files.readAllBytes(Paths.get(infoPath)), StandardCharsets.UTF_8);
                    Matcher matcher = tsNumPat.matcher(info);
                    if (matcher.find()) {
                        List<Integer> tsNum = JSON.parseArray(matcher.group(1), Integer.class);
                        tsNum.set(0, tsNum.get(0) + 1);
                        info = info.replaceAll(tsNumPat.toString(), TS_NUM + JSON.toJSONString(tsNum) + "\n");
                    }
                    randomAccessFile.write(info.getBytes(StandardCharsets.UTF_8));
                } finally {
                    fileLock.release();
                }
            }
        } catch (IOException e) {
            log.error("improveVideoPost fail,basePath:{},line:{},index:{}", basePath, line, index, e);
        }
    }

    private Result<String> downloadAndSaveTS(M3U8BO m3u8BO, VideoResolver videoResolver) {
        try {
            TsListBO tsListBO = videoResolver.getTsList(m3u8BO);
            List<TsBO> tsList = tsListBO.getTsList();
            Assert.isNotEmpty(tsList, "ts地址列表为空");

            String fileId = m3u8BO.getId();
            String basePath = videoBasePath + "/" + fileId;
            Assert.isTrue(FileUtil.deleteFolder(basePath), basePath + "删除失败");

            Files.createDirectories(Paths.get(basePath + "/ts"));

            String m3u8Content = m3u8BO.getContent();
            Path originM3U8Path = Paths.get(basePath + "/origin.m3u8");
            Files.write(originM3U8Path, m3u8Content.getBytes(), StandardOpenOption.CREATE_NEW);

            UnaryOperator<byte[]> decodeTsFunction = getDecodeTsFunction(basePath, tsListBO, videoResolver);

            SynchronousQueue<LocalTsBO> synchronousQueue = new SynchronousQueue<>();
            batchSubmitTsTask(basePath, tsList, decodeTsFunction, synchronousQueue);
            List<LocalTsBO> localTsList = getFutureAndSaveInfo(basePath, m3u8BO, tsList.size(), synchronousQueue);

            Path localM3U8Path = Paths.get(basePath + "/local.m3u8");
            String newM3U8Content = buildLocalM3U8Content(tsListBO, fileId, localTsList);
            Files.write(localM3U8Path, newM3U8Content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE_NEW);

            return Result.success(fileId);
        } catch (InterruptedException e) {
            log.info("downloadAndSaveTS interrupt m3u8BO name:{}", m3u8BO.getName());
            return Result.fail("下载任务中断");
        } catch (ViewException e) {
            throw e;
        } catch (Exception e) {
            log.error("downloadAndSaveTS fail m3u8BO name:{}", m3u8BO.getName(), e);
            return Result.fail("下载保存视频发生错误");
        }
    }

    private UnaryOperator<byte[]> getDecodeTsFunction(String basePath, TsListBO tsListBO, VideoResolver videoResolver) throws IOException {
        TsEncBO tsEncBO = tsListBO.getTsEncBO();
        if (tsEncBO != null) {
            String head = tsListBO.getHead();
            byte[] encKey = tsEncBO.getEncKey();
            Path encKeyPath = Paths.get(basePath + "/enc.key");
            Files.write(encKeyPath, encKey, StandardOpenOption.CREATE_NEW);
            UnaryOperator<byte[]> decodeTsFunction = videoResolver.getDecodeTsFunction(head, encKey);
            if (decodeTsFunction != null) {
                tsListBO.setHead(head.replaceAll(encKeyPat.toString(), ""));
                return decodeTsFunction;
            }
        }
        return UnaryOperator.identity();
    }

    private void batchSubmitTsTask(String basePath, List<TsBO> tsList, UnaryOperator<byte[]> decodeFun, SynchronousQueue<LocalTsBO> synchronousQueue) {
        IntStream.range(0, tsList.size()).forEach(i -> {
            TsBO tsBO = tsList.get(i);
            String localTsName = i + ".ts";

            LocalTsBO localTsBO = new LocalTsBO();
            localTsBO.setIndex(i);
            localTsBO.setTsUrl(tsBO.getUrl());
            localTsBO.setTsExt(tsBO.getExt());
            localTsBO.setLocalTsName(localTsName);
            localTsBO.setLocalTsPath(basePath + "/ts/" + localTsName);
            downloadTSPool.submit(() -> doDownloadAndSaveTS(localTsBO, decodeFun, synchronousQueue));
        });
    }

    private List<LocalTsBO> getFutureAndSaveInfo(String basePath, M3U8BO m3u8BO, int allSize, SynchronousQueue<LocalTsBO> synchronousQueue) throws IOException, InterruptedException {
        try (RandomAccessFile rf = new RandomAccessFile(basePath + "/info.txt", "rw")) {
            String head = "文件名：" + m3u8BO.getName() + "\n";
            head += "来源：" + m3u8BO.getSourceUrl() + "\n";
            head += "进度：";
            rf.write(head.getBytes(StandardCharsets.UTF_8));

            int preRateByteNum = 0;
            AtomicInteger errorTsNum = new AtomicInteger();
            List<LocalTsBO> localTsList = new ArrayList<>(allSize);
            for (int i = 0; i < allSize; i++) {
                LocalTsBO localTsBO = synchronousQueue.take();
                rf.seek(rf.getFilePointer() - preRateByteNum);
                String rate = String.format("%.2f", (i + 1) * 100.0 / allSize) + "%";
                byte[] rateByte = rate.getBytes(StandardCharsets.UTF_8);
                rf.write(rateByte);
                preRateByteNum = rateByte.length;
                localTsList.add(localTsBO);
                Optional.of(localTsBO).filter(ts -> !ts.isTaskSuccess()).ifPresent(ts -> errorTsNum.getAndIncrement());
            }
            List<Integer> tsNum = Arrays.asList(allSize - errorTsNum.get(), allSize);
            rf.write(("\n" + TS_NUM + JSON.toJSONString(tsNum) + "\n").getBytes(StandardCharsets.UTF_8));
            localTsList.sort(Comparator.comparing(LocalTsBO::getIndex));
            return localTsList;
        }
    }

    private String buildLocalM3U8Content(TsListBO tsListBO, String fileId, List<LocalTsBO> localTsList) {
        return localTsList.stream().map(localTsBO -> {
            String tsUrl = localTsBO.getTsUrl();
            String tsExt = localTsBO.getTsExt();
            String index = localTsBO.getIndex() + "";
            String localTsName = localTsBO.getLocalTsName();
            String localTsPath = "/video/ts/" + fileId + "/" + localTsName;
            //HLS以 # 字母开头的行是注释和 TAG，其中 TAG 必须是 #EXT 开头
            //所以加注释的话不能以#EXT 开头。将下载失败的文件用特殊注释前缀标识，等重新下载完成后在将特殊注释替换
            if (!localTsBO.isTaskSuccess()) {
                List<String> tsMetadata = Arrays.asList(index, tsUrl);
                localTsPath = HOMER_HLS_TS_PRE + JSON.toJSONString(tsMetadata);

                tsExt = Arrays.stream(tsExt.split("\n")).map(inf ->
                        HOMER_HLS_PRE + index + "-" + inf).collect(Collectors.joining("\n"));
            }
            return tsExt + "\n" + localTsPath;
        }).collect(Collectors.joining("\n", tsListBO.getHead(), tsListBO.getEnd()));
    }

    private void doDownloadAndSaveTS(LocalTsBO localTsBO, UnaryOperator<byte[]> decodeFunction, SynchronousQueue<LocalTsBO> synchronousQueue) {
        try {
            localTsBO.setTaskSuccess(doDownloadAndSaveTS(localTsBO.getTsUrl(), localTsBO.getLocalTsPath(), decodeFunction));
        } finally {
            putSynchronousQueue(synchronousQueue, localTsBO);
        }
    }

    private boolean doDownloadAndSaveTS(String tsUrl, String localTsPath, UnaryOperator<byte[]> decodeFunction) {
        if (Thread.currentThread().isInterrupted()) {
            return false;
        }
        try {
            HttpRespBO respBO = HttpUtil.httpGet10(tsUrl);
            if (respBO == null) {
                log.error("downloadTS fail tsUrl:{}", tsUrl);
                return false;
            }
            try (RandomAccessFile ts = new RandomAccessFile(localTsPath, "rw")) {
                ts.write(decodeFunction.apply(respBO.getBody()));
                return true;
            }
        } catch (IOException e) {
            log.error("save ts fail,tsUrl:{},localTsPath:{}", tsUrl, localTsPath, e);
        }
        return false;
    }

    private void putSynchronousQueue(SynchronousQueue<LocalTsBO> synchronousQueue, LocalTsBO localTsBO) {
        try {
            synchronousQueue.put(localTsBO);
        } catch (InterruptedException ignored) {
            //synchronousQueue的操作会清除中断位
        }
    }

    @EventListener
    public void handleMyEvent(StopTsTaskEvent event) {
        this.downloadTSPool.shutdownNow();
        this.downloadTSPool = getDownloadTSPool();
    }

    @Lookup("downloadTSPool")
    public ExecutorService getDownloadTSPool() {
        return null;
    }


    @Data
    private static class LocalTsBO {
        /**
         * ts的位置
         */
        private int index;

        /**
         * ts的地址
         */
        private String tsUrl;

        /**
         * ts tag描述
         */
        private String tsExt;

        /**
         * 本地ts名称
         */
        private String localTsName;

        /**
         * 本地ts路径
         */
        private String localTsPath;

        /**
         * 任务是否成功
         */
        private boolean taskSuccess;
    }
}
