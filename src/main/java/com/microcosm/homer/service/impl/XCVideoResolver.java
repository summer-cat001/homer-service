package com.microcosm.homer.service.impl;

import com.microcosm.homer.model.video.*;
import com.microcosm.homer.service.CommonVideoResolver;
import com.microcosm.homer.utils.AESUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author summer
 * @date 2023-12-01 11:48
 * 星辰影院/策驰影院
 */
@Slf4j
@Service("xcVideoResolver")
public class XCVideoResolver extends CommonVideoResolver {

    private static final Pattern m3u8UrlPat = Pattern.compile("\"url\":\"(.*?)\"");
    private static final Pattern m3u8NamePat = Pattern.compile("<title>(.*?)</title>");
    private static final Pattern encKeyPat = Pattern.compile("#EXT-X-KEY:METHOD=(.*?),.*?IV=(.*?)\n");
    private static final Pattern searchPat = Pattern.compile("<a class=\"img-pic\"[\\s\\S]*?href=\"(.*?)\"[\\s\\S]*?title=\"(.*?)\"[\\s\\S]*?data-original=\"(.*?)\"");
    private static final Pattern videoSourcePat = Pattern.compile("<a href=\"javascript:void[\\s\\S]*?id=\"#(.*?)\"[\\s\\S]*?>(.*?)</a>");
    private static final Pattern videoLiPat = Pattern.compile("[^']<li[\\s\\S]*?href=\"(.*?)\">(.*?)</a>");

    public XCVideoResolver() {
        super("http://yuanxiuge.com", "星辰影院");
    }

    @Override
    public boolean support(String url) {
        return url != null && url.contains("yuanxiuge.com");
    }

    @Override
    public UnaryOperator<byte[]> getDecodeTsFunction(String head, byte[] encKey) {
        Matcher encKeyMatcher = encKeyPat.matcher(head);
        if (encKeyMatcher.find()) {
            String method = encKeyMatcher.group(1);
            return method.contains("aes") || method.contains("AES") ?
                    encByte -> AESUtil.decode(encByte, encKey, "0000000000000000") : null;
        }
        return null;
    }

    @Override
    protected Pattern getM3U8UrlPat() {
        return m3u8UrlPat;
    }

    @Override
    protected Pattern getM3U8NamePat() {
        return m3u8NamePat;
    }

    @Override
    protected String resolveSearchContent(String content) {
        return content;
    }

    @Override
    protected List<SearchItemBO> resolveSearchItemList(String searchContent) {
        return resolveSearchItemListTemplate(searchContent, searchPat, matcher -> matcher.group(1)
                , matcher -> matcher.group(2), matcher -> matcher.group(3), UnaryOperator.identity());
    }

    @Override
    protected List<SearchSourceItemBO> resolveNetVideoSource(String searchItemPage) {
        return resolvePlayListNetVideoSource(searchItemPage
                , videoSourcePat, matcher -> matcher.group(2), matcher -> matcher.group(1));
    }

    @Override
    protected List<NetVideoBO> resolveNetVideo(String sourceContent) {
        return resolveNetVideoTemplate(sourceContent, videoLiPat
                , matcher -> matcher.group(1), matcher -> matcher.group(2));
    }
}
