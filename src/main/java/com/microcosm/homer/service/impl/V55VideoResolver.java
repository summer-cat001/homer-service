package com.microcosm.homer.service.impl;

import com.microcosm.homer.model.video.NetVideoBO;
import com.microcosm.homer.model.video.SearchItemBO;
import com.microcosm.homer.model.video.SearchSourceItemBO;
import com.microcosm.homer.service.CommonVideoResolver;
import com.microcosm.homer.utils.AESUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author summer
 * @date 2023-12-01 11:48
 * 55影院
 */
@Slf4j
@Service("v55VideoResolver")
public class V55VideoResolver extends CommonVideoResolver {

    private static final Pattern m3u8UrlPat = Pattern.compile("var player_aaaa[\\s\\S]*?\"url\":\"(.*?)\"");
    private static final Pattern m3u8NamePat = Pattern.compile("<title>(.*?)</title>");
    private static final Pattern searchPat = Pattern.compile("<div class=\"thumb\">[\\s\\S]*?href=\"(.*?)\" title=\"(.*?)\"[\\s\\S]*?data-original=\"(.*?)\"");
    private static final Pattern videoSourcePat = Pattern.compile("<div class=\"stui-pannel-box b playlist mb\">[\\s\\S]*?<h3 class=\"title\">[\\s\\S]*?/>(.*?)</h3>[\\s\\S]*?</ul>");
    private static final Pattern videoLiPat = Pattern.compile("<a href=\"(.*?)\">(.*?)</a>");

    public V55VideoResolver() {
        super("http://www.5555kan.com", "55影院（有广告）");
    }

    @Override
    public boolean support(String url) {
        return url != null && url.contains("5555kan.com");
    }

    @Override
    protected String getSearchUrl(String keyword) {
        return sourceUrl + "/search/-------------.html?wd=" + keyword;
    }

    @Override
    protected Pattern getM3U8UrlPat() {
        return m3u8UrlPat;
    }

    @Override
    protected Pattern getM3U8NamePat() {
        return m3u8NamePat;
    }

    @Override
    protected String resolveSearchContent(String content) {
        return content;
    }

    @Override
    protected List<SearchItemBO> resolveSearchItemList(String searchContent) {
        return resolveSearchItemListTemplate(searchContent, searchPat, matcher -> matcher.group(1)
                , matcher -> matcher.group(2), matcher -> matcher.group(3), UnaryOperator.identity());
    }

    @Override
    protected List<SearchSourceItemBO> resolveNetVideoSource(String searchItemPage) {
        return resolveNetVideoSourceTemplate(searchItemPage, videoSourcePat
                , matcher -> matcher.group(1), Matcher::group, UnaryOperator.identity());
    }

    @Override
    protected List<NetVideoBO> resolveNetVideo(String sourceContent) {
        return resolveNetVideoTemplate(sourceContent, videoLiPat
                , matcher -> matcher.group(1), matcher -> matcher.group(2));
    }
}
