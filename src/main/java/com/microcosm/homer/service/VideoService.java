package com.microcosm.homer.service;

import com.microcosm.homer.model.Result;
import com.microcosm.homer.model.video.LocalVideoInfoBO;
import com.microcosm.homer.model.video.NetVideoSearchBO;
import com.microcosm.homer.model.video.TsAsyncTaskBO;
import com.microcosm.homer.model.video.TsAsyncTaskVO;

import java.util.List;

/**
 * @author summer
 * @date 2023-12-05 15:52
 */
public interface VideoService {

    /**
     * 获取文件字节数组
     *
     * @param relativePath 文件相对地址
     * @return ts文件字节数组
     */
    Result<byte[]> getFileByte(String relativePath);

    /**
     * 获取本地视频info内容
     *
     * @param videoId 视屏id
     * @return 本地视频info内容
     */
    Result<LocalVideoInfoBO> getLocalVideoInfo(String videoId);

    /**
     * 搜索视频
     *
     * @param keyword 关键字
     * @return 各平台搜索结果
     */
    Result<List<NetVideoSearchBO>> searchVideo(String keyword);

    /**
     * 爬取视频
     *
     * @param url 地址
     * @return 视频id
     */
    Result<String> crawlVideo(String url);

    /**
     * 删除视频
     *
     * @param videoId 视频id
     * @return Result
     */
    Result<Void> deleteVideo(String videoId);

    /**
     * 完善视频
     *
     * @param videoId 视频id
     * @return Result
     */
    Result<Void> improveVideo(String videoId);

    /**
     * 添加下载任务列表
     *
     * @param url 视频播放页url
     * @return Result
     */
    Result<String> addCrawlVideoAsync(String url);

    /**
     * 取消下载任务列表
     *
     * @param fileId 文件id
     * @return Result
     */
    Result<Void> rmCrawlVideoAsync(String fileId);

    /**
     * 获取下载任务列表
     *
     * @return 下载任务列表
     */
    Result<List<TsAsyncTaskVO>> getTsAsyncTaskList();

    /**
     * 获取本地视频列表
     *
     * @return 视频列表
     */
    Result<List<LocalVideoInfoBO>> getVideoList();
}
