package com.microcosm.homer.service;

import com.microcosm.homer.model.ActionBO;
import com.microcosm.homer.model.DeviceDescBO;
import com.microcosm.homer.model.Result;
import com.microcosm.homer.model.video.PlayTaskBO;

import java.util.List;

/**
 * @author summer
 * @date 2022-04-21 19:54
 */
public interface DeviceService {

    /**
     * 获取设备描述
     *
     * @param desUrl 设备描述url
     * @return Result
     */
    Result<DeviceDescBO> getDeviceDesc(String desUrl);

    /**
     * 设置播放资源
     *
     * @param actionBO 动作
     * @return Result
     */
    Result<Void> setResourceUrl(ActionBO actionBO);

    /**
     * 播放资源
     *
     * @param actionBO 动作
     * @return Result
     */
    Result<Void> playResource(ActionBO actionBO);

    /**
     * 获取播放进度
     *
     * @param actionBO 动作
     * @return Result
     */
    Result<String> getPositionInfo(ActionBO actionBO);

    /**
     * 播放视频
     *
     * @param deviceId 设备id
     * @param videoId  视频id
     * @return Result
     */
    Result<Void> playVideo(int deviceId, String videoId);

    /**
     * 设置视频播放任务
     *
     * @param videoId 视频id
     * @return Result
     */
    Result<Void> addDevicePlayTask(String videoId);

    /**
     * 移除视频播放任务
     *
     * @param videoId 视频id
     * @param index   任务位置
     * @return Result
     */
    Result<Void> rmDevicePlayTask(String videoId, int index);

    /**
     * 调整视频播放任务顺序
     *
     * @param videoId 视频id
     * @param index   任务位置
     * @param action  调整动作，1：向下，-1:向上
     * @return Result
     */
    Result<Void> changeDevicePlayTaskOrder(String videoId, int index, int action);

    /**
     * 播放第一个视频播放任务
     *
     * @param deviceId 设备id
     * @return Result
     */
    Result<Void> playPlayTask(int deviceId);

    /**
     * 获取视频播放任务
     *
     * @return Result
     */
    Result<List<PlayTaskBO>> getTaskList();
}
