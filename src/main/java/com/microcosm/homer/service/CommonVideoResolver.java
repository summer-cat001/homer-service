package com.microcosm.homer.service;

import com.microcosm.homer.model.HttpRespBO;
import com.microcosm.homer.model.video.*;
import com.microcosm.homer.utils.*;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author summer
 * @date 2023-12-11 12:00
 */
@Slf4j
public abstract class CommonVideoResolver implements VideoResolver {

    protected String sourceUrl;
    protected String sourceName;

    private static final Pattern tsContentPat = Pattern.compile("(#[\\s\\S]*?)\n([^#].*?\\.ts)");
    public static final Pattern encKeyPat = Pattern.compile("#EXT-X-KEY.*?URI=\"(.*?)\"[^\n]*");
    private static final Pattern m3u8ContentPat = Pattern.compile("([\\s\\S]*?)(#EXTINF[\\s\\S]*\\.ts)([\\s\\S]*)");

    protected CommonVideoResolver(String sourceUrl, String sourceName) {
        this.sourceUrl = sourceUrl;
        this.sourceName = sourceName;
    }

    /**
     * 获取解析m3u8的正则模式
     *
     * @return Pattern
     */
    protected abstract Pattern getM3U8UrlPat();

    /**
     * 获取解析视频名的正则模式
     *
     * @return Pattern
     */
    protected abstract Pattern getM3U8NamePat();

    /**
     * 根据搜索返回的结果解析搜索到的列表内容模版
     *
     * @return String
     */
    protected abstract String resolveSearchContent(String content);

    /**
     * 根据搜索到的列表内容模版解析出搜索到的列表
     *
     * @param searchContent 搜索到的列表内容
     * @return List<SearchItemBO>
     */
    protected abstract List<SearchItemBO> resolveSearchItemList(String searchContent);

    /**
     * 根据搜索到的详情页解析出所有来源内容
     *
     * @param searchItemPage 视频页面
     * @return List<SearchSourceItemBO>
     */
    protected abstract List<SearchSourceItemBO> resolveNetVideoSource(String searchItemPage);

    /**
     * 根据来源内容解析该来源的所有视频
     *
     * @param sourceContent 详情页解析出的来源内容
     * @return List<NetVideoSourceBO>
     */
    protected abstract List<NetVideoBO> resolveNetVideo(String sourceContent);

    public Map<String, String> getDefaultHeaderMap() {
        return null;
    }

    protected String getSearchUrl(String keyword) {
        return sourceUrl + "/search/?wd=" + keyword;
    }

    @Override
    public NetVideoSearchBO search(String keyword) {
        List<NetVideoGroupBO> netVideoGroupList = new ArrayList<>();
        NetVideoSearchBO netVideoSearchBO = new NetVideoSearchBO();
        netVideoSearchBO.setNetVideoGroupList(netVideoGroupList);
        netVideoSearchBO.setSourceName(sourceName);
        netVideoSearchBO.setSourceUrl(sourceUrl);

        String searchUrl = getSearchUrl(keyword);
        if (searchUrl == null) {
            log.error("getSearchUrl fail class:{} keyword:{}", this.getClass().getName(), keyword);
            return netVideoSearchBO;
        }
        HttpRespBO respBO = HttpUtil.httpGet(searchUrl, 3, getDefaultHeaderMap());
        if (respBO == null || !respBO.ok()) {
            log.error(" search wd fail respBO:{}", respBO);
            return netVideoSearchBO;
        }
        String res = respBO.getUTF8Body();
        String searchContent = resolveSearchContent(res);
        if (searchContent == null) {
            return netVideoSearchBO;
        }
        List<SearchItemBO> searchItemList = resolveSearchItemList(searchContent);
        searchItemList.stream().map(searchItemBO -> {
            String searchItemUrl = searchItemBO.getUrl();
            HttpRespBO searchItemPageBO = HttpUtil.httpGet(searchItemUrl, 3, getDefaultHeaderMap());
            if (searchItemPageBO == null || !searchItemPageBO.ok()) {
                log.error("searchItemPage fail url:{},searchItemPageBO:{}", searchItemUrl, searchItemPageBO);
                return null;
            }
            String searchItemPage = searchItemPageBO.getUTF8Body();
            List<SearchSourceItemBO> searchSourceItemList = resolveNetVideoSource(searchItemPage);
            if (searchSourceItemList == null) {
                return null;
            }
            NetVideoGroupBO netVideoGroupBO = new NetVideoGroupBO();
            netVideoGroupBO.setName(searchItemBO.getName());
            netVideoGroupBO.setCoverUrl(searchItemBO.getCoverUrl());
            netVideoGroupBO.setNetVideoSourceList(buildNetVideoSourceList(searchSourceItemList));
            return netVideoGroupBO;
        }).filter(Objects::nonNull).forEach(netVideoGroupList::add);

        return netVideoSearchBO;
    }

    @Override
    public boolean support(String url) {
        return url != null && url.startsWith(sourceUrl);
    }

    @Override
    public M3U8BO getM3U8(String url) {
        HttpRespBO respBO = HttpUtil.httpGet10(url, getDefaultHeaderMap());
        Assert.isTrue(respBO, "获取M3U8文件失败", () ->
                log.error("CommonVideoResolver getM3U8 fail url:{}", url));

        String res = respBO.getUTF8Body();
        Matcher matcher = getM3U8UrlPat().matcher(res);
        Assert.isTrue(matcher.find(), "未获取到M3U8地址");

        String m3u8Url = matcher.group(1);
        HttpRespBO m3u8Resp = HttpUtil.httpGet10(m3u8Url, getDefaultHeaderMap());
        Assert.isTrue(m3u8Resp, "获取M3U8列表文件失败", () ->
                log.error("CommonVideoResolver getM3U8 list fail m3u8Url:{},m3u8Resp:{}", m3u8Url, m3u8Resp));

        String m3u8Content = m3u8Resp.getUTF8Body();
        Assert.isTrue(DataUtils.isNotEmpty(m3u8Content) && m3u8Content.contains("#EXTM3U")
                , "M3U8文件内容错误", () -> log.error("CommonVideoResolver M3U8 content error m3u8Url:{},m3u8Content:{}", m3u8Url, m3u8Content));
        RealM3U8BO realM3U8BO = HLSUtil.getMaxBandwidthM3U8(m3u8Url, m3u8Content);

        Matcher m3u8NameMatcher = getM3U8NamePat().matcher(res);
        String title = Optional.of(m3u8NameMatcher).filter(Matcher::find).map(m -> m.group(1)).orElse(null);

        String id = UUID.randomUUID().toString().replace("-", "");
        return new M3U8BO(id, title, realM3U8BO.getM3u8Content(), url, realM3U8BO.getM3u8Url());
    }

    @Override
    public TsListBO getTsList(M3U8BO m3u8BO) {
        String videoId = m3u8BO.getId();
        String m3u8Url = m3u8BO.getM3u8Url();
        String m3u8Content = m3u8BO.getContent();
        Matcher m3u8Matcher = m3u8ContentPat.matcher(m3u8Content);
        Assert.isTrue(m3u8Matcher.find(), "m3u8内容解析失败");

        String head = m3u8Matcher.group(1);
        StringBuilder newHead = new StringBuilder(head);
        TsEncBO tsEncBO = buildTsEnc(videoId, newHead, m3u8Url);
        List<TsBO> tsList = new ArrayList<>();

        Matcher tsMatcher = tsContentPat.matcher(m3u8Matcher.group(2));
        while (tsMatcher.find()) {
            String url = tsMatcher.group(2);
            String realUrl = NetUtil.resolvePostFromPre(m3u8Url, url);
            tsList.add(new TsBO(realUrl, tsMatcher.group(1)));
        }
        TsListBO tsListBO = new TsListBO();
        tsListBO.setTsList(tsList);
        tsListBO.setTsEncBO(tsEncBO);
        tsListBO.setHead(newHead.toString());
        tsListBO.setEnd(m3u8Matcher.group(3));
        return tsListBO;
    }

    protected List<SearchItemBO> resolveSearchItemListTemplate(String searchContent, Pattern searchPat
            , Function<Matcher, String> getUrl, Function<Matcher, String> getName, Function<Matcher, String> getCoverUrl, UnaryOperator<SearchItemBO> handleSearchItemBO) {
        List<SearchItemBO> searchItemList = new ArrayList<>();
        Matcher searchMatcher = searchPat.matcher(searchContent);
        while (searchMatcher.find()) {
            SearchItemBO searchItemBO = new SearchItemBO();
            searchItemBO.setName(getName.apply(searchMatcher));
            searchItemBO.setCoverUrl(getCoverUrl.apply(searchMatcher));
            searchItemBO.setUrl(NetUtil.composeUrl(sourceUrl, getUrl.apply(searchMatcher)));

            searchItemBO = handleSearchItemBO.apply(searchItemBO);
            Optional.ofNullable(searchItemBO).ifPresent(searchItemList::add);
        }
        return searchItemList;
    }

    protected List<SearchSourceItemBO> resolvePlayListNetVideoSource(String searchItemPage
            , Pattern videoSourcePat, Function<Matcher, String> getName, Function<Matcher, String> getSourceContent) {
        return resolveNetVideoSourceTemplate(searchItemPage, videoSourcePat, getName, getSourceContent, searchSourceItemBO -> {
            String playListId = searchSourceItemBO.getSourceContent();
            Pattern videoUlPat = Pattern.compile("id=\"" + playListId + "\">[\\s\\S]*?</ul>");
            Matcher ulMatcher = videoUlPat.matcher(searchItemPage);
            String sourceContent = Optional.of(ulMatcher).filter(Matcher::find).map(Matcher::group).orElse("");
            searchSourceItemBO.setSourceContent(sourceContent);
            return searchSourceItemBO;
        });
    }

    protected List<SearchSourceItemBO> resolveNetVideoSourceTemplate(String searchItemPage, Pattern videoSourcePat
            , Function<Matcher, String> getName, Function<Matcher, String> getSourceContent, UnaryOperator<SearchSourceItemBO> handleSearchSourceItemBO) {
        List<SearchSourceItemBO> searchSourceItemList = new ArrayList<>();
        Matcher matcher = videoSourcePat.matcher(searchItemPage);
        while (matcher.find()) {
            SearchSourceItemBO searchSourceItemBO = new SearchSourceItemBO();
            searchSourceItemBO.setName(getName.apply(matcher));
            searchSourceItemBO.setSourceContent(getSourceContent.apply(matcher));

            searchSourceItemBO = handleSearchSourceItemBO.apply(searchSourceItemBO);
            Optional.ofNullable(searchSourceItemBO).ifPresent(searchSourceItemList::add);
        }
        return searchSourceItemList;
    }

    protected List<NetVideoBO> resolveNetVideoTemplate(String sourceContent, Pattern videoLiPat
            , Function<Matcher, String> getUrl, Function<Matcher, String> getName) {
        List<NetVideoBO> netVideoList = new ArrayList<>();
        Matcher sourceMatcher = videoLiPat.matcher(sourceContent);
        while (sourceMatcher.find()) {
            String itemUrl = getUrl.apply(sourceMatcher);
            NetVideoBO netVideoBO = new NetVideoBO();
            netVideoBO.setName(getName.apply(sourceMatcher));
            netVideoBO.setUrl(NetUtil.composeUrl(sourceUrl, itemUrl));
            netVideoList.add(netVideoBO);
        }
        return netVideoList;
    }

    private TsEncBO buildTsEnc(String videoId, StringBuilder newHead, String m3u8Url) {
        TsEncBO tsEncBO = null;
        String head = newHead.toString();
        Matcher encKeyMatcher = encKeyPat.matcher(head);
        if (encKeyMatcher.find()) {
            String originEncKeyUrl = encKeyMatcher.group(1);
            String encKeyUrl;
            if (originEncKeyUrl.startsWith("http")) {
                encKeyUrl = originEncKeyUrl;
            } else if (originEncKeyUrl.startsWith("/")) {
                encKeyUrl = NetUtil.resolveRootUrl(m3u8Url) + originEncKeyUrl;
            } else {
                encKeyUrl = m3u8Url.substring(0, m3u8Url.lastIndexOf("/") + 1) + originEncKeyUrl;
            }
            HttpRespBO encKeyResp = HttpUtil.httpGet10(encKeyUrl);
            Assert.isTrue(encKeyResp, "获取ts文件密钥失败", () ->
                    log.error("getEncKey fail encKeyUrl:{},encKeyResp:{}", encKeyUrl, encKeyResp));

            newHead.setLength(0);
            newHead.append(head.replace(originEncKeyUrl, "/video/enc/key/" + videoId));

            byte[] encKey = encKeyResp.getBody();
            tsEncBO = new TsEncBO();
            tsEncBO.setEncKey(encKey);
            tsEncBO.setEncKeyUrl(encKeyUrl);
            tsEncBO.setOriginEncKeyUrl(originEncKeyUrl);
        }
        return tsEncBO;
    }

    private List<NetVideoSourceBO> buildNetVideoSourceList(List<SearchSourceItemBO> searchSourceItemList) {
        return searchSourceItemList.stream().filter(s -> s.getSourceContent() != null).map(searchSourceItemBO -> {
            String sourceContent = searchSourceItemBO.getSourceContent();
            List<NetVideoBO> netVideoList = resolveNetVideo(sourceContent);

            NetVideoSourceBO netVideoSourceBO = new NetVideoSourceBO();
            netVideoSourceBO.setName(searchSourceItemBO.getName());
            netVideoSourceBO.setNetVideoList(Optional.ofNullable(netVideoList).orElseGet(ArrayList::new));
            return netVideoSourceBO;
        }).collect(Collectors.toList());
    }
}
