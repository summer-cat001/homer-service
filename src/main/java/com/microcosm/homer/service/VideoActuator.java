package com.microcosm.homer.service;

import com.microcosm.homer.model.Result;
import com.microcosm.homer.model.video.LocalVideoInfoBO;
import com.microcosm.homer.model.video.TsAsyncTaskBO;

/**
 * @author summer
 * @date 2023-12-05 14:41
 * 视频执行器 以桥接模式代替继承
 */
public interface VideoActuator {

    String TS_NUM = "tsNum:";
    String HOMER_HLS_PRE = "#HOMER-";
    String HOMER_HLS_TS_PRE = HOMER_HLS_PRE + "TS:";

    /**
     * 下载并保存视频
     *
     * @param url           视频所在页面地址
     * @param videoResolver 视频解析器
     * @return Result<Long> 保存到本地的视频id
     */
    Result<String> downloadAndSaveTS(String url, VideoResolver videoResolver);

    /**
     * 生成下载并保存视频任务
     *
     * @param url           视频所在页面地址
     * @param videoResolver 视频解析器
     * @return Result<TsAsyncTaskBO> 任务
     */
    Result<TsAsyncTaskBO> downloadAndSaveTSAsync(String url, VideoResolver videoResolver);

    /**
     * 完善视频
     *
     * @param localVideoInfoBO 本地视频信息
     * @param videoResolver    视频解析器
     */
    void improveVideo(LocalVideoInfoBO localVideoInfoBO, VideoResolver videoResolver);

    /**
     * 视频是否正在完善中
     *
     * @param videoId 视频id
     */
    boolean isImproveIng(String videoId);
}
