package com.microcosm.homer.service;

import com.microcosm.homer.model.video.M3U8BO;
import com.microcosm.homer.model.video.NetVideoSearchBO;
import com.microcosm.homer.model.video.TsListBO;

import java.util.function.UnaryOperator;

/**
 * @author summer
 * @date 2023-11-30 17:52
 * 视频解析器
 */
public interface VideoResolver {

    /**
     * 搜索视频
     *
     * @param keyword 关键字
     * @return NetVideoSearchBO
     */
    NetVideoSearchBO search(String keyword);

    /**
     * 是否支持解析
     *
     * @param url 地址
     * @return boolean
     */
    boolean support(String url);

    /**
     * 获取m3u8文件
     *
     * @param url 地址
     * @return Result
     */
    M3U8BO getM3U8(String url);

    /**
     * 获取ts地址列表
     *
     * @param m3u8BO m3u8内容
     * @return Result
     */
    TsListBO getTsList(M3U8BO m3u8BO);

    /**
     * 获取解密ts的方法（可选）
     * 返回不为null的话，就会对视频进行解密，并把本地m3u8文件头中的密钥信息清除
     * 返回为null的话，不是解密视频，会将源m3u8文件头中的密钥下载下来，并把本地m3u8文件头中的密钥信息替换为本地的密钥，由播放设备解密
     *
     * @param head   m3u8文件头
     * @param encKey 加密的key
     * @return UnaryOperator
     */
    default UnaryOperator<byte[]> getDecodeTsFunction(String head, byte[] encKey) {
        return null;
    }
}
