package com.microcosm.homer.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * @author summer
 * @date 2023-11-30 20:26
 */
@Slf4j
public class FileUtil {

    private FileUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static boolean deleteFolder(String path) {
        Path folder = Paths.get(path);
        if (Files.exists(folder)) {
            try (Stream<Path> stream = Files.walk(folder)) {
                return stream.allMatch(f -> {
                    try {
                        return Files.deleteIfExists(f);
                    } catch (IOException e) {
                        log.error("deleteFolder fail f:{}", f, e);
                    }
                    return false;
                });
            } catch (Exception e) {
                log.error("deleteFolder fail path:{}", path, e);
            }
        }
        return true;
    }
}
