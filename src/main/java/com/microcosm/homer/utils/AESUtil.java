package com.microcosm.homer.utils;

import com.microcosm.homer.enums.ResultEnum;
import com.microcosm.homer.exception.ViewException;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author summer
 * @date 2023-12-01 17:35
 */
@Slf4j
public class AESUtil {

    private AESUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * AES 对称加密（RSA非对称加密）
     * CBC 有向量 （ECB 无向量）
     * PKCS5Padding 填充模式（NoPadding 无填充）
     */
    private static final String ALG_AES_CBC_PKCS5 = "AES/CBC/PKCS5Padding";

    private static final String ALGORITHM = "AES";

    /**
     * 解密方法
     *
     * @param encrypted1 加密字节数组
     * @return 解密后的字节数组(UTF8编码)
     * @throws Exception 异常
     */
    public static byte[] decode(byte[] encrypted1, byte[] aesKey, String aesIv) {
        try {
            // step 1 获得一个密码器
            Cipher cipher = Cipher.getInstance(ALG_AES_CBC_PKCS5);
            // step 2 初始化密码器，指定是加密还是解密(Cipher.DECRYPT_MODE 解密; Cipher.ENCRYPT_MODE 加密)
            // 加密时使用的盐来够造秘钥对象
            SecretKeySpec skeySpec = new SecretKeySpec(aesKey, ALGORITHM);
            // 加密时使用的向量，16位字符串
            IvParameterSpec iv = new IvParameterSpec(aesIv.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            // 解密后的报文数组
            byte[] original = cipher.doFinal(encrypted1);
            // 输出utf8编码的字符串，输出字符串需要指定编码格式
            return original;
        } catch (Exception e) {
            log.error("AESUtil decode fail", e);
            throw new ViewException(ResultEnum.FAIL.getCode(), "AES解密失败");
        }
    }

    /**
     * 加密
     *
     * @param plainText 明文
     * @return Base64编码的密文
     * @throws Exception 加密异常
     */
    public static String encode(String plainText, String aesKey, String aesIv) throws Exception {
        Cipher cipher = Cipher.getInstance(ALG_AES_CBC_PKCS5);
        SecretKeySpec skeySpec = new SecretKeySpec(aesKey.getBytes(), ALGORITHM);
        IvParameterSpec iv = new IvParameterSpec(aesIv.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        // 这里的编码格式需要与解密编码一致
        byte[] encryptText = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(encryptText);
    }

    public static void main(String[] args) {
        String plainText = "明文报文，进行对称AES算法加密传输";
        String cipherStr;
        try {
            System.out.println("被加解密的报文:[ " + plainText + " ]");
            cipherStr = AESUtil.encode(plainText, "1234567890123456", "0000000000000000");
            System.out.println("AES 加密后的Base64报文:[ " + cipherStr + "]");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
