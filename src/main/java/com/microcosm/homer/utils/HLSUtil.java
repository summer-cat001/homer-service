package com.microcosm.homer.utils;

import com.microcosm.homer.model.HttpRespBO;
import com.microcosm.homer.model.video.RealM3U8BO;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author summer
 * @date 2023-12-07 15:51
 */
@Slf4j
public class HLSUtil {

    private static final Pattern m3u8ExtPat = Pattern.compile(
            "#EXT-X-STREAM-INF:.*?BANDWIDTH=(.*?)(,.*?(\r\n|\n)|(\r\n|\n))(.*?\\.m3u8)");

    private HLSUtil() {
        throw new IllegalStateException("Utility class");
    }

    //获取最大分辨率的m3u8内容
    public static RealM3U8BO getMaxBandwidthM3U8(String m3u8Url, String m3u8Content) {
        if (m3u8Content.contains("#EXT-X-STREAM-INF")) {
            long maxBandwidth = 0;
            String extM3U8Url = null;
            Matcher extMatcher = m3u8ExtPat.matcher(m3u8Content);
            while (extMatcher.find()) {
                long bandwidth = Long.parseLong(extMatcher.group(1));
                if (bandwidth > maxBandwidth) {
                    maxBandwidth = bandwidth;
                    extM3U8Url = extMatcher.group(5).trim();
                }
            }
            Assert.isNotEmpty(extM3U8Url, "获取M3U8详情地址失败");

            String finalM3u8Url = m3u8Url = NetUtil.resolvePostFromPre(m3u8Url, extM3U8Url);
            HttpRespBO m3u8DetailResp = HttpUtil.httpGet10(m3u8Url);
            Assert.isTrue(m3u8DetailResp, "获取M3U8内容详情失败", () ->
                    log.error("getMaxBandwidthM3U8 detail fail m3u8Url:{},m3u8DetailResp:{}", finalM3u8Url, m3u8DetailResp));
            m3u8Content = m3u8DetailResp.getUTF8Body();
        }
        return new RealM3U8BO(m3u8Url, m3u8Content);
    }
}
