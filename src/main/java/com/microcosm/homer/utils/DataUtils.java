package com.microcosm.homer.utils;

import java.util.*;

/**
 * @author summer
 * @date 2021-10-12 14:46
 */
public class DataUtils {

    private DataUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static boolean isEmpty(Object o) {
        return o == null ||
                (o instanceof Map && ((Map) o).size() == 0) ||
                (o instanceof Object[] && ((Object[]) o).length == 0) ||
                (o instanceof Collection && ((Collection) o).size() == 0) ||
                (o instanceof String && ((String) o).trim().length() == 0);
    }

    public static boolean isNotEmpty(Object o) {
        return !isEmpty(o);
    }
}