package com.microcosm.homer.utils;


import com.microcosm.homer.enums.ResultEnum;
import com.microcosm.homer.exception.ViewException;
import com.microcosm.homer.model.HttpRespBO;
import com.microcosm.homer.model.Result;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author summer
 * @date 2021-10-12 12:00
 */
public class Assert {

    private Assert() {
        throw new IllegalStateException("Utility class");
    }

    public static void isNotEmpty(Object o, String message) {
        isTrue(DataUtils.isNotEmpty(o), ResultEnum.FAIL, message);
    }

    public static void isNotEmpty(Object o, ResultEnum resultCode, String message) {
        isTrue(DataUtils.isNotEmpty(o), resultCode, message);
    }

    public static void isEmpty(Object o, ResultEnum resultCode, String message) {
        isTrue(DataUtils.isEmpty(o), resultCode, message);
    }

    public static void isSuccess(Result<?> result, Runnable... rs) {
        isTrue(result.isSuccess(), result.getCode(), result.getMessage(), rs);
    }

    public static void isTrue(HttpRespBO respBO, String message, Runnable... rs) {
        isTrue(respBO != null && respBO.ok(), ResultEnum.FAIL, message, rs);
    }

    public static void isTrue(boolean expression, String message, Runnable... rs) {
        isTrue(expression, ResultEnum.FAIL, message, rs);
    }

    public static void isTrue(boolean expression, ResultEnum resultCode, String message, Runnable... rs) {
        isTrue(expression, resultCode.getCode(), message, rs);
    }

    public static void isTrue(boolean expression, int code, String message, Runnable... rs) {
        if (!expression) {
            Optional.ofNullable(rs).ifPresent(r -> Arrays.stream(r).forEach(Runnable::run));
            throw new ViewException(code, message);
        }
    }
}
