package com.microcosm.homer.utils;


/**
 * @author summer
 * @date 2023-12-07 10:46
 */
public class HandlerUtil {

    private HandlerUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static void branchHandler(boolean predicate, Runnable sucHandler, Runnable failHandler) {
        if (predicate) {
            sucHandler.run();
        } else {
            failHandler.run();
        }
    }

}
