package com.microcosm.homer.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author summer
 * @date 2021-10-12 13:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ViewException extends RuntimeException {

    private final int code;

    public ViewException(int code, String message) {
        super(message);
        this.code = code;
    }
}
