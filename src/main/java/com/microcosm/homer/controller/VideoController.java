package com.microcosm.homer.controller;

import com.alibaba.fastjson.JSON;
import com.microcosm.homer.model.*;
import com.microcosm.homer.model.video.LocalVideoInfoBO;
import com.microcosm.homer.model.video.NetVideoSearchBO;
import com.microcosm.homer.model.video.TsAsyncTaskVO;
import com.microcosm.homer.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author summer
 * @date 2022-04-20 15:19
 */
@Slf4j
@RestController
@RequestMapping("/video")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @GetMapping(value = "/m3u8/{videoId}", produces = "application/vnd.apple.mpegurl")
    public byte[] getM3U8(@PathVariable String videoId) {
        return returnFileTemplate(videoId + "/local.m3u8");
    }

    @GetMapping(value = "/ts/{videoId}/{tsName}", produces = "video/mp2t")
    public byte[] getTs(@PathVariable String videoId, @PathVariable String tsName) {
        return returnFileTemplate(videoId + "/ts/" + tsName);
    }

    @GetMapping(value = "/enc/key/{videoId}", produces = "application/octet-stream")
    public byte[] getEncKey(@PathVariable String videoId) {
        return returnFileTemplate(videoId + "/enc.key");
    }

    @GetMapping("/search")
    public Result<List<NetVideoSearchBO>> searchVideo(String keyword) {
        return videoService.searchVideo(keyword);
    }

    @PostMapping("/crawl")
    public Result<String> crawlVideo(String url) {
        return videoService.crawlVideo(url);
    }

    @PostMapping("/delete")
    public Result<Void> deleteVideo(String videoId) {
        return videoService.deleteVideo(videoId);
    }

    @PostMapping("/improve")
    public Result<Void> improveVideo(String videoId) {
        return videoService.improveVideo(videoId);
    }

    @PostMapping("/crawl/task/add")
    public Result<String> addCrawlTask(String url) {
        return videoService.addCrawlVideoAsync(url);
    }

    @PostMapping("/crawl/task/rm")
    public Result<Void> rmCrawlTask(String fileId) {
        return videoService.rmCrawlVideoAsync(fileId);
    }

    @GetMapping("/crawl/task/list")
    public Result<List<TsAsyncTaskVO>> getTsAsyncTaskList() {
        return videoService.getTsAsyncTaskList();
    }

    @GetMapping("/list")
    public Result<List<LocalVideoInfoBO>> getVideoList() {
        return videoService.getVideoList();
    }

    private byte[] returnFileTemplate(String relativePath) {
        Result<byte[]> result = videoService.getFileByte(relativePath);
        return Optional.of(result).filter(Result::isSuccess).map(Result::getData).orElseGet(() -> JSON.toJSONBytes(result));
    }
}
