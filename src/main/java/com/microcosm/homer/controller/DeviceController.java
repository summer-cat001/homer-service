package com.microcosm.homer.controller;

import com.microcosm.homer.enums.SSDPStEnum;
import com.microcosm.homer.model.*;
import com.microcosm.homer.model.video.PlayTaskBO;
import com.microcosm.homer.service.DeviceService;
import com.microcosm.homer.service.SSDPService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

import static com.microcosm.homer.enums.ResultEnum.PARM_SERVER_TYPE_FAIL;

/**
 * @author summer
 * @date 2022-05-01 21:29
 */
@Slf4j
@RestController
@RequestMapping("/device")
public class DeviceController {

    @Autowired
    private SSDPService ssdpService;

    @Autowired
    private DeviceService deviceService;

    @Value("${play.delay}")
    private int playDelay;

    private int deviceId;

    private final Lock lock = new ReentrantLock();

    @GetMapping("/search")
    public Result<List<DeviceDescBO>> search(String serviceType) {
        return ssdpServiceTemp(serviceType, ssdpService::discover);
    }

    @GetMapping("/notify/start")
    public Result<Void> notifyStart(String serviceType) {
        return ssdpServiceTemp(serviceType, ssdpService::receiveNotify);
    }

    @GetMapping("/notify/stop")
    public Result<Void> notifyStop(String serviceType) {
        return ssdpServiceTemp(serviceType, ssdpService::stopReceiveNotify);
    }

    @GetMapping("/play")
    public Result<Void> playVideo(int deviceId, String videoId) {
        return deviceService.playVideo(deviceId, videoId);
    }

    @GetMapping("/play/task/add")
    public Result<Void> addDevicePlayTask(String videoId) {
        return deviceService.addDevicePlayTask(videoId);
    }

    @GetMapping("/play/task/rm")
    public Result<Void> rmDevicePlayTask(String videoId, int index) {
        return deviceService.rmDevicePlayTask(videoId, index);
    }

    @GetMapping("/play/task/order/change")
    public Result<Void> changeDevicePlayTaskOrder(String videoId, int index, int action) {
        return deviceService.changeDevicePlayTaskOrder(videoId, index, action);
    }

    @GetMapping("/play/task/play")
    public Result<Void> playPlayTask(int deviceId) {
        this.deviceId = deviceId;
        return deviceService.playPlayTask(deviceId);
    }

    @GetMapping("/play/task/list")
    public Result<List<PlayTaskBO>> getTaskList() {
        return deviceService.getTaskList();
    }


    @RequestMapping("/callback")
    public void callback(HttpServletRequest request) throws IOException {
        int len = request.getContentLength();
        ServletInputStream iii = request.getInputStream();
        byte[] buffer = new byte[len];
        iii.read(buffer, 0, len);
        String body = new String(buffer);
        if (body.contains("STOPPED")) {
            playNext();
        }
    }

    private void playNext() {
        if (lock.tryLock()) {
            try {
                //等10秒再播放，防止设备关闭不了（部分设备关机需要在几秒内没操作才行）
                Thread.sleep(playDelay);
                deviceService.playPlayTask(deviceId);
            } catch (Exception e) {
                log.error("playNext error", e);
            } finally {
                lock.unlock();
            }
        }
    }

    private <T> Result<T> ssdpServiceTemp(String serviceType, Function<SSDPStEnum, Result<T>> mapper) {
        return Optional.ofNullable(serviceType).map(SSDPStEnum::getEnumByType)
                .map(mapper).orElseGet(() -> Result.fail(PARM_SERVER_TYPE_FAIL));
    }
}
