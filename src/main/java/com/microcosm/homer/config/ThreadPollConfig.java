package com.microcosm.homer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author summer
 * @date 2023-11-30 20:52
 */
@Slf4j
@Configuration
public class ThreadPollConfig {

    private AtomicInteger downloadTSPoolI;
    private AtomicInteger searchVideoPoolI;
    private AtomicInteger improveVideoI;

    @Bean("downloadTSPool")
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ExecutorService downloadTSPool() {
        log.info("downloadTSPool init success");
        downloadTSPoolI = new AtomicInteger();
        return new ThreadPoolExecutor(100, 100, 0L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(), this::downloadTSFactory, new ThreadPoolExecutor.CallerRunsPolicy());
    }

    @Bean("searchVideoPool")
    public ExecutorService searchVideoPool() {
        searchVideoPoolI = new AtomicInteger();
        return new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS,
                new SynchronousQueue<>(), this::searchVideoPoolFactory, new ThreadPoolExecutor.CallerRunsPolicy());
    }

    @Bean("improveVideoPool")
    public ExecutorService improveVideo() {
        improveVideoI = new AtomicInteger();
        return new ThreadPoolExecutor(100, 100, 0L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(), this::improveVideoFactory, new ThreadPoolExecutor.CallerRunsPolicy());
    }

    private Thread downloadTSFactory(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        t.setName("downloadTSPool-" + downloadTSPoolI.getAndIncrement());
        return t;
    }

    private Thread improveVideoFactory(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        t.setName("improveVideoPool-" + improveVideoI.getAndIncrement());
        return t;
    }

    private Thread searchVideoPoolFactory(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        t.setName("searchVideoPool-" + searchVideoPoolI.getAndIncrement());
        return t;
    }
}
