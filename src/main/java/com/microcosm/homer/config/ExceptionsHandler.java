package com.microcosm.homer.config;

import com.microcosm.homer.exception.ViewException;
import com.microcosm.homer.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author summer
 * @date 2021-10-12 13:52
 */
@Slf4j
@RestControllerAdvice
public class ExceptionsHandler {
    /**
     * 异常包装成view
     */
    @ExceptionHandler(ViewException.class)
    public Result<Void> runtimeExceptionHandler(ViewException e) {
        return Result.fail(e.getCode(), e.getMessage());
    }
}
