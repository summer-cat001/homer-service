package com.microcosm.homer.config;

import com.microcosm.homer.model.DeviceDescBO;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

/**
 * @author summer
 * @date 2023-12-08 20:07
 */
@Slf4j
@Component
public class Context {

    @Getter
    private String localHost;

    @Setter
    @Getter
    private List<DeviceDescBO> deviceDescList;

    @Value("${server.port}")
    private String serverPort;

    @PostConstruct
    public void init() throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getLocalHost();
        String localIp = inetAddress.getHostAddress();
        localHost = "http://" + localIp + ":" + serverPort;
        log.info("当前地址为：" + localHost + "/html/index.html");
    }

}
