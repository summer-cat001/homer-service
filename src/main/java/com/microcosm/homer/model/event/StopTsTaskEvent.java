package com.microcosm.homer.model.event;

import org.springframework.context.ApplicationEvent;

public class StopTsTaskEvent extends ApplicationEvent {
    public StopTsTaskEvent(Object source) {
        super(source);
    }
}
