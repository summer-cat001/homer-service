package com.microcosm.homer.model.video;

import lombok.Data;

import java.util.List;

/**
 * @author summer
 * @date 2022-05-02 22:13
 */
@Data
public class TsListBO {
    /**
     * ts列表
     */
    private List<TsBO> tsList;

    /**
     * 文件头
     */
    private String head;

    /**
     * 文件尾
     */
    private String end;

    /**
     * ts加密信息
     */
    private TsEncBO tsEncBO;
}
