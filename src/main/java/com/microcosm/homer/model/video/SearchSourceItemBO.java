package com.microcosm.homer.model.video;

import lombok.Data;

/**
 * @author summer
 * @date 2024-01-15 16:22
 */
@Data
public class SearchSourceItemBO {
    /**
     * 来源名
     */
    private String name;

    /**
     * 来源内容
     */
    private String sourceContent;
}
