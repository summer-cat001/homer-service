package com.microcosm.homer.model.video;

import lombok.Data;

/**
 * @author summer
 * @date 2024-01-15 16:22
 */
@Data
public class SearchItemBO {
    /**
     * 地址
     */
    private String url;

    /**
     * 名称
     */
    private String name;

    /**
     * 封面地址
     */
    private String coverUrl;
}
