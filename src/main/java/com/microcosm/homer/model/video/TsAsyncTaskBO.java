package com.microcosm.homer.model.video;

import lombok.Data;

/**
 * @author summer
 * @date 2024-01-12 18:17
 */
@Data
public class TsAsyncTaskBO {

    /**
     * 文件id
     */
    private String fileId;

    /**
     * 视频名
     */
    private String name;

    /**
     * 视频地址
     */
    private String url;

    /**
     * 任务
     */
    private Runnable runnable;
}
