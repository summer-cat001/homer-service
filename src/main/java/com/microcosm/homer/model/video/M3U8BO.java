package com.microcosm.homer.model.video;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author summer
 * @date 2022-05-02 22:13
 */
@Data
@AllArgsConstructor
public class M3U8BO {
    /**
     * 视频id
     */
    private String id;
    
    /**
     * 视频名
     */
    private String name;

    /**
     * m3u8内容
     */
    private String content;

    /**
     * 来源
     */
    private String sourceUrl;

    /**
     * m3u8地址
     */
    private String m3u8Url;
}
