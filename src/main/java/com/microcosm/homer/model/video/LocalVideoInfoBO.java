package com.microcosm.homer.model.video;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.List;

/**
 * @author summer
 * @date 2024-01-10 18:33
 */
@Data
public class LocalVideoInfoBO {
    /**
     * 视频id
     */
    private String id;

    /**
     * 视频名
     */
    private String name;

    /**
     * 来源
     */
    private String source;

    /**
     * 进度
     */
    private String rate;

    /**
     * ts个数 第一位是成功的个数，第二位是总数
     */
    private List<Integer> tsNum;

    /**
     * 视频是否正在完善中
     */
    private boolean improveIng;

    public void setTsNum(String tsNumJson) {
        this.tsNum = JSON.parseArray(tsNumJson, Integer.class);
    }
}
