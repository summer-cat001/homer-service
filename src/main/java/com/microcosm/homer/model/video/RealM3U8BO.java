package com.microcosm.homer.model.video;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RealM3U8BO {
    /**
     * m3u8地址
     */
    private String m3u8Url;

    /**
     * m3u8内容
     */
    private String m3u8Content;
}
