package com.microcosm.homer.model.video;

import lombok.Data;

/**
 * @author summer
 * @date 2024-01-12 18:17
 */
@Data
public class PlayTaskBO {

    /**
     * 视频id
     */
    private String videoId;

    /**
     * 视频名
     */
    private String name;
}
