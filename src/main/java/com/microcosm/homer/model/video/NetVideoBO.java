package com.microcosm.homer.model.video;

import lombok.Data;

/**
 * @author summer
 * @date 2024-01-10 17:34
 */
@Data
public class NetVideoBO {
    /**
     * 名称
     */
    private String name;

    /**
     * 视频播放页地址
     */
    private String url;
}
