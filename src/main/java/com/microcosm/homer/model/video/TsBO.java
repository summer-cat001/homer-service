package com.microcosm.homer.model.video;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author summer
 * @date 2023-12-05 14:46
 */
@Data
@AllArgsConstructor
public class TsBO {
    /**
     * ts完整地址
     */
    private String url;

    /**
     * ts tag描述
     */
    private String ext;
}
