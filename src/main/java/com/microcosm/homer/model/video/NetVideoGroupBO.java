package com.microcosm.homer.model.video;

import lombok.Data;

import java.util.List;

/**
 * @author summer
 * @date 2024-01-10 17:33
 */
@Data
public class NetVideoGroupBO {
    /**
     * 名称
     */
    private String name;

    /**
     * 封面地址
     */
    private String coverUrl;

    /**
     * 视频列表
     */
    private List<NetVideoSourceBO> netVideoSourceList;
}
