package com.microcosm.homer.model.video;

import lombok.Data;

import java.util.List;

/**
 * @author summer
 * @date 2024-01-15 17:03
 */
@Data
public class NetVideoSourceBO {
    /**
     * 来源名
     */
    private String name;

    /**
     * 视频列表
     */
    private List<NetVideoBO> netVideoList;
}
