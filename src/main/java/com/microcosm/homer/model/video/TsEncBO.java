package com.microcosm.homer.model.video;

import lombok.Data;

/**
 * @author summer
 * @date 2023-12-06 18:04
 */
@Data
public class TsEncBO {
    /**
     * 加密key的地址
     */
    private String encKeyUrl;

    /**
     * 源加密key的地址
     */
    private String originEncKeyUrl;

    /**
     * 加密key
     */
    private byte[] encKey;
}
