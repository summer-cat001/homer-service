package com.microcosm.homer.model.video;

import lombok.Data;

import java.util.List;

/**
 * @author summer
 * @date 2024-01-10 17:56
 */
@Data
public class NetVideoSearchBO {
    /**
     * 来源名
     */
    private String sourceName;

    /**
     * 来源地址
     */
    private String sourceUrl;

    /**
     * 剧集列表
     */
    private List<NetVideoGroupBO> netVideoGroupList;
}
