package com.microcosm.homer.model;

import lombok.Data;

/**
 * @author summer
 * @date 2023-11-30 17:23
 */
@Data
public class DevicePlayVO {
    /**
     * 设备地址
     */
    private String deviceUrl;

    /**
     * 服务类型
     */
    private String serviceType;

    /**
     * 向服务发出控制消息的URL
     */
    private String controlUrl;

    /**
     * 资源url
     */
    private String resourceUrl;

}
